
// @file
//   ReamMe Information

  ===========================================
   AnyTM (Any Team Management System) ReadMe
  ===========================================

  ---------------------------------------------------------------------------------------
   This document describes briefly the AnyTM (Any Team Management System) for Drupal 6.x
  ---------------------------------------------------------------------------------------

  AnyTM (pronounced Any Tea-M) stands for Any Team Management System. It is a Drupal 
  (drupal.org) 6.x Installation Profile that aims to help you simply set up a powerful 
  custom website for organising and managing Any Team, from a sports clubs to a business.

  This installation profile will install and configure modules and content for your team
  Drupal website. It allows you to select a theme, configure blocks, set up users, polls
  a shout box and forums and pages. When done with the install you will have a site
  configured specifically for your team up and running.

  The most up to date information regarding the project and any related information can be
  found at the Drupal project page.

  Install instructions are in INSTALL.txt
  License information is in LICENSE.txt

  =================================================
   AnyTM (Any Team Management System) Team Credits
  =================================================

  David Doyle (Project Manager)
  Matthew Rowles
  Eddie Evers
  Graham Morton
  Adam Mather
  Ashley Maher (Project Supervisor)