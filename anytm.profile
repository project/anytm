<?php

/*
 * @file
 *   AnyTM (pronounced Any Tea-M) stands for Any Team Management System.
 *   It is a Drupal (drupal.org) Installation Profile that aims to help
 *   you simply set up a powerful custom website for organising and
 *   managing Any Team, from a sports clubs to a business.
 */

/*
 * Return the array of the modules to be enabled by the AnyTM installation profile
 *
 * @return
 *   Array of modules
 */

function anytm_profile_modules() {
  return array(
    // Enable required core modules first.
    'block', 'filter', 'node', 'system', 'user', 'taxonomy',

    // Enable optional core modules without dependancies
    'color', 'comment', 'help', 'locale', 'menu', 'poll', 'search',

    // Enable optional core modules with dependancies
    'forum',

    // Enable install profile helper module
    'install_profile_api',

    // Enable any contributed modules without dependancies
    'advanced_help',
    'shoutbox',

    // Enable any contributed modules with dependancies

    // Enable View modules
    'views',
    'views_ui',     // Depends on views

    // Enable CCK modules
    'content',
    'content_copy',         // Depends on content
    'content_permissions',  // Depends on content
    'content_copy',         // Depends on content
    'content_permissions',  // Depends on content
    'text',                 // Depends on content

    // Enable Date modules
    'date_api',       // No dependancies
    'date_locale',    // Depends locale
    'date_timezone',  // Depends on date_api
    'date_php4',      // Depends on date_api
    'date_repeat',    // Depends on date_api
    'date_popup',     // Depends on date_api and date_timezone
    'date',           // Depends on content, date_api and date_timezone
    'date_tools',     // Depends on content, date_api and date_timezone and date

    // Enable Calender modules
    'calendar',       // Depends on views, date_api, date_timezone
    'jcalendar',      // Depends on calendar
    'calendar_ical',  // Depends on calendar
  );
}

/*
 * Returns description of installation profile for installation screen.
 *
 * @return
 *   An array(with keys 'name' and 'description') describing the AnyTM profile
 */

function anytm_profile_details() {
  // Return description for the installation profile
  return array(
    'name' => st('ANYTM - Any Team Management System'),
    'description' => st(
      '<img width = 142 height = 82 src = "http://img21.imageshack.us/img21/6759/logoef.png">'.
      '<img width = 142 height = 82 src = "http://img21.imageshack.us/img21/6759/logoef.png">'.
      '<img width = 142 height = 82 src = "http://img21.imageshack.us/img21/6759/logoef.png">'.
      '<h1><br />Select the <a href = "http://sourceforge.net/projects/anytm/">AnyTM</a> '.
      'Installation Profile to enable a Team Management System suitable for AnyTMs needs.</h1>'
    ),
  );
}

/*
 * Return a list of tasks that the AnyTM installation profile will perform
 *
 * @return
 *   A keyed array of tasks the AnyTM installation profile will perform
 */

function anytm_profile_task_list() {
  // Array of task titles that will be completed
  return array(
    'task0' => st('Introduction'),
    'task1' => st('Content Setup'),
    'task2' => st('Theme Selection'),
    'task3' => st('Block Configuration'),
    'task4' => st('Time Zone Configuration'),
    'task5' => st('User Setup 1'),
    'task6' => st('User Setup 2'),
    'task7' => st('Shoutbox Setup'),
    'task8' => st('Poll Setup'),
    'task9' => st('Forum Setup'),
    'task10' => st('Page Setup'),
    'task11' => st('Final Step'),
  );
}

/*
 * Perform installation tasks for this profile.
 *
 * @param $task
 *   The current $task of the install system.
 * @param $url
 *   Complete URL to be used for a link or form action
 *
 * @return
 *   An optional HTML string to display to the user. Only used if you
 *   modify the $task, otherwise discarded.
 */

function anytm_profile_tasks(&$task, $url) {
  // Set up install_profile_api module includes
  install_include(anytm_profile_modules());

  if ($task == 'profile') {
    $task = 'task0';
  }

  // Introduction Task
  if ($task == 'task0') {
    $task0_output = drupal_get_form('anytm_intro_form', $url);

    if (!variable_get('anytm_intro_finished', FALSE)) {
      drupal_set_title(st('Introduction'));
      return $task0_output;
    }
    else {
      $task = 'task1';
    }
  }

  // Content Setup task
  if ($task == 'task1') {
    $task1_output = drupal_get_form('anytm_content_form', $url);

    if (!variable_get('anytm_content_setup_finished', FALSE)) {
      drupal_set_title(st('Content Setup'));
      return $task1_output;
    }
    else {
      $task = 'task2';
    }
  }

  // Theme Selection task
  if ($task == 'task2') {
    $task2_output = drupal_get_form('anytm_theme_selection_form', $url);

    if (!variable_get('anytm_theme_selection_finished', FALSE)) {
      drupal_set_title(st('Theme Selection'));
      return $task2_output;
    }
    else {
      $task = 'task3';
    }
  }

  // Block Configuration task
  if ($task == 'task3') {
    $task3_output = drupal_get_form('anytm_block_configuration_form', $url);

    if (!variable_get('anytm_block_configuration_finished', FALSE)) {
      drupal_set_title(st('Block Configuration'));
      return $task3_output;
    }
    else {
      $task = 'task4';
    }
  }

  // Time Zone Configuration Task
  if ($task == 'task4') {
    $task4_output = drupal_get_form('anytm_date_configuration_form', $url);

    if (!variable_get('anytm_date_configuration_finished', FALSE)) {
      drupal_set_title(st('Time Zone Configuration'));
      return $task4_output;
    }
    else {
      $task = 'task5';
    }
  }

  // User Setup Task 1 (Check if user wants to add more users)
  if ($task == 'task5') {
    $task5_output = drupal_get_form('anytm_user_setup1_form', $url);

    if (!variable_get('anytm_user_setup1_finished', FALSE)) {
      drupal_set_title(st('User Setup 1'));
      return $task5_output;
    }
    else {
      $task = 'task6';
    }
  }

  // User Setup Task 2 (Add more users)
  if ($task == 'task6') {
    if (variable_get('anytm_setup_users', 1) == 1) {
      $task6_output = drupal_get_form('anytm_user_setup2_form', $url);

      if (!variable_get('anytm_user_setup2_finished', FALSE)) {
        drupal_set_title(st('User Setup 2'));
        return $task6_output;
      }
      else {
        $task = 'task7';
      }
    }
    else {
      $task = 'task7';
    }
  }

  // Shoutbox Setup Task
  if ($task == 'task7') {
    if (variable_get('anytm_block_shoutbox_enabled', 1) == 1) {
      $task7_output = drupal_get_form('anytm_shoutbox_form', $url);

      if (!variable_get('anytm_shoutbox_finished', FALSE)) {
        drupal_set_title(st('Shoutbox Setup'));
        return $task7_output;
      }
      else {
        $task = 'task8';
      }
    }
    else {
      $task = 'task8';
    }
  }

  // Poll Setup Task
  if ($task == 'task8') {
    if (variable_get('anytm_block_poll_enabled', 1) == 1) {
      $task8_output = drupal_get_form('anytm_poll_form', $url);

      if (!variable_get('anytm_poll_finished', FALSE)) {
        drupal_set_title(st('Poll Setup'));
        return $task8_output;
      }
      else {
        $task = 'task9';
      }
    }
    else {
      $task = 'task9';
    }
  }

  // Forum Setup Task
  if ($task == 'task9') {
    if (variable_get('anytm_block_forum_enabled' , 1) == 1) {
      $task9_output = drupal_get_form('anytm_forum_form', $url);

      if (!variable_get('anytm_forum_finished', FALSE)) {
        drupal_set_title(st('Forum Setup'));
        return $task9_output;
      }
      else {
        $task = 'task10';
      }
    }
    else {
      $task = 'task10';
    }
  }

  // Page Setup Task
  if ($task == 'task10') {
    $task10_output = drupal_get_form('anytm_frontpage_form', $url);

    if (!variable_get('anytm_frontpage_finished', FALSE)) {
      drupal_set_title(st('Page Setup'));
      return $task10_output;
    }
    else {
      $task = 'task11';
    }
  }

  // Final Configuration Tasks
  if ($task == 'task11') {
    $task11_output = drupal_get_form('anytm_final_form', $url);

    if (!variable_get('anytm_final_finished', FALSE)) {
      drupal_set_title(st('Final Step'));
      return $task11_output;
    }
    else {
      anytm_cleanup();
      $task = 'profile-finished';
    }
  }
}

/*
 * Perform intro tasks for this profile.
 *
 * @param $task
 *   The current $task of the install system.
 * @param $url
 *   Complete URL to be used for a link or form action
 *
 * @return
 *   Form displaying introduction message
 */

function anytm_intro_form(&$form_state, $url) {
  // Intro Message
  $intro_info = st('<p>This is the introduction to your installation process. Before you start make sure ');
  $intro_info .= st('you have everything you need. You will need to set up a theme, configure your blocks');
  $intro_info .= st(', select your time zone, you can optionally set up any users (entering usernames and');
  $intro_info .= st(' emails), and also add a shoutbox, poll, and forums. Finally you can add pages to your site');
  $intro_info .= st(' (e.g. Home, About, Contact, etc). When you have the information needed click continue.</p>');

  $form['intro_info'] = array(
    '#type' => 'markup',
    '#value' => $intro_info,
  );

  // Continue Button
  $form['continue'] = array('#type' => 'submit', '#value' => st('Continue'));
  $form['errors'] = array();
  $form['#action'] = $url;
  $form['#redirect'] = FALSE;

  return $form;
}

/*
 * Insert nodes, custom content & views, and menus
 * Set a variable to signal the intro form has been submitted
 *
 * @param $task
 *   The current $task of the install system.
 * @param $url
 *   Complete URL to be used for a link or form action
 *
 */

function anytm_intro_form_submit($form, &$form_state) {
  $types = array(
    array(
      'type' => 'page',
      'name' => st('Page'),
      'module' => 'node',
      'description' => st("A <em>page</em>, is a simple method for creating and displaying information that rarely changes, such as an \"About us\" section of a website. By default, a <em>page</em> entry does not allow visitor comments and is not featured on the site's initial home page."),
      'custom' => TRUE,
      'modified' => TRUE,
      'locked' => FALSE,
      'help' => '',
      'min_word_count' => '',
    ),
    array(
      'type' => 'newsitem',
      'name' => st('News Item'),
      'module' => 'node',
      'description' => st("A <em>News Item</em>, similar in form to a <em>page</em>, is ideal for creating and displaying news content"),
      'custom' => TRUE,
      'modified' => TRUE,
      'locked' => FALSE,
      'help' => '',
      'min_word_count' => '',
    ),
  );

  foreach ($types as $type) {
    $type = (object) _node_type_set_defaults($type);
    node_type_save($type);
  }

  anytm_include_calendar_field();   // Install custom calendar node type & fields
  anytm_include_calendar_view();    // Install customised calendar view
  anytm_include_latestnews_view();  // Install custom latest news view

  // Create menu items

  $array = array(
    'attributes' => array(
      'title' => 'Calendar',
    ),
  );
  $array = serialize($array);

  db_query("INSERT INTO {menu_links} VALUES ('primary-links', NULL , 0, 'calendar', 'calendar',
    'Calendar', '%s', 'menu', 0, 0, 0, 0, 20, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)", $array
  );

  $array = array(
    'attributes' => array(
      'title' => 'Forums',
    ),
  );
  $array = serialize($array);

  db_query("INSERT INTO {menu_links} VALUES ('primary-links', NULL , 0, 'forum', 'forum',
    'Forums', '%s', 'menu', 0, 0, 0, 0, 15, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)", $array
  );

  $array = array(
    'attributes' => array(
      'title' => 'Create Content',
    ),
  );
  $array = serialize($array);

  db_query("INSERT INTO {menu_links} VALUES ('primary-links', NULL , 0, 'node/add', 'node/add',
    'Create Content', '%s', 'menu', 0, 0, 0, 0, 10, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)", $array
  );

  $array = array(
    'attributes' => array(
      'title' => 'Nodes',
    ),
  );
  $array = serialize($array);

  db_query("INSERT INTO {menu_links} VALUES ('primary-links', NULL , 0, 'node', 'node',
    'Nodes', '%s', 'menu', 0, 0, 0, 0, 5, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)", $array
  );

  $array = array(
    'attributes' => array(
      'title' => 'Admin Help',
    ),
  );
  $array = serialize($array);

  db_query("INSERT INTO {menu_links} VALUES ('primary-links', NULL , 0, 'admin/help', 'admin/help',
    'Admin Help', '%s', 'menu', 0, 0, 0, 0, 25, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)", $array
  );

  // Update the menu router information.
  menu_rebuild();

  variable_set('anytm_intro_finished', TRUE);  // Set form finished variable
}

/*
 * Perform content tasks for this profile.
 *
 * @param $task
 *   The current $task of the install system.
 * @param $url
 *   Complete URL to be used for a link or form action
 *
 * @return
 *   Form explaining the content message after installing custom content & views
 */

function anytm_content_form(&$form_state, $url) {
  // Content setup information
  $form['intro'] = array(
    '#type' => 'markup',
    '#value' => st('<p>As it states above, we have set up custom content types. Click to move on.</p>'),
  );

  // Continue button
  $form['continue'] = array('#type' => 'submit', '#value' => st('Continue'));
  $form['errors'] = array();
  $form['#action'] = $url;
  $form['#redirect'] = FALSE;

  return $form;
}

/*
 * Set a variable to signal the content form has been submitted
 *
 * @param $task
 *   The current $task of the install system.
 * @param $url
 *   Complete URL to be used for a link or form action
 *
 */

function anytm_content_form_submit($form, &$form_state) {
  variable_set('anytm_content_setup_finished', TRUE);  // Set form finished variable
}

/*
 * Perform theme tasks for this profile.
 *
 * @param $task
 *   The current $task of the install system.
 * @param $url
 *   Complete URL to be used for a link or form action
 *
 * @return
 *   Form for detailing available themes and selecting one
 */

function anytm_theme_selection_form(&$form_state, $url) {
  // Setup an array with available theme information

  $themes = db_query("SELECT name, info FROM {system} WHERE type='theme'");
  variable_set('anytm_themes', $themes);

  $theme_array = array();
  while ($theme_row = db_fetch_array($themes)) {
    $theme_array[] = $theme_row;
  }

  variable_set('anytm_theme_array', $theme_array);
  $theme_options_array = array();

  $theme_info = array();
  foreach ($theme_array as $theme_array_value) {
    $theme_key = $theme_array_value['name'];
    $theme_info = unserialize($theme_array_value['info']);
    $theme_options_array[$theme_key] =
      $theme_info['name'] ."<p>" .
      $theme_info['description'] ."</p><p><img src = \"" .
      $theme_info['screenshot'] ."\"></p>";
  }

  variable_set('anytm_theme_options_array', $theme_options_array);

  // Theme options
  $form['theme_selection_form_radio'] = array(
    '#type' => 'radios',
    '#title' => st('Select a Theme for your Team Management System'),
    '#options' => $theme_options_array,
  );

  // Continue button
  $form['continue'] = array(
    '#type' => 'submit',
    '#value' => st('Continue'),
  );

  $form['errors'] = array();
  $form['#action'] = $url;
  $form['#redirect'] = FALSE;

  return $form;
}

/*
 * Set the default theme
 * Set a variable to signal the theme form has been submitted
 *
 * @param $task
 *   The current $task of the install system.
 * @param $url
 *   Complete URL to be used for a link or form action
 *
 */

function anytm_theme_selection_form_submit($form, &$form_state) {
  // Get theme form value
  $theme_selection = $form_state['values']['theme_selection_form_radio'];
  // Update themes in database
  db_query("UPDATE {system} SET status=0 WHERE type='theme'");
  db_query("UPDATE {system} SET status=1 WHERE name='%s'", $theme_selection);
  // Set chosen theme variable
  variable_set('theme_default', $theme_selection);
  variable_set('anytm_theme_selection_finished', TRUE);  // Set form finished variable
}

/*
 * Perform block configuration tasks for this profile.
 *
 * @param $task
 *   The current $task of the install system.
 * @param $url
 *   Complete URL to be used for a link or form action
 *
 * @return
 *   Form for enabling blocks and configuring their placement
 */

function anytm_block_configuration_form(&$form_state, $url) {
  // Get theme and theme regions from database
  $theme = variable_get('theme_default', 'garland');
  $info = db_query("SELECT info FROM {system} WHERE name='%s'", $theme);
  $info = db_fetch_object($info);
  $info = get_object_vars($info);
  $info = $info['info'];
  $info = unserialize($info);
  $options_array = $info['regions'];
  variable_set('anytm_placement_array', $options_array);

  // Block configuration intro info
  $form_intro = st('<p>This page provides a interface for enabling and assigning blocks to regions.');
  $form_intro .= st(' To enable a block tick its corresponding check box. To assign a block to a');
  $form_intro .= st(' region, select the region you want to place it in from it\'s corresponding');
  $form_intro .= st('drop down box. Not all themes implement the same regions, or display regions');
  $form_intro .= st(' in the same way, so the options available to select in the drop down box ');
  $form_intro .= st('are gathered on a per-theme basis. Click continue when you have configured ');
  $form_intro .= st('all the blocks you want for your team website.</p>');

  $form['intro'] = array(
    '#type' => 'markup',
    '#value' => $form_intro,
  );

  // Block Options

  $form['block_form_calendar_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('<b>Enable Calendar</b>'),
  );

  $form['block_form_calendar_placement'] = array(
    '#type' => 'select',
    '#default_value' => NULL,
    '#title' => st('Place Calendar'),
    '#options' => $options_array,
  );

  $form['new_line_01'] = array(
    '#type' => 'markup',
    '#value' => st('<br />'),
  );

  $form['block_form_drupal_footer_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('<b>Enable Drupal Footer</b>'),
  );

  $form['block_form_drupal_footer_placement'] = array(
    '#type' => 'select',
    '#default_value' => NULL,
    '#title' => st('Place Drupal Footer'),
    '#options' => $options_array,
  );

  $form['new_line_02'] = array(
    '#type' => 'markup',
    '#value' => st('<br />'),
  );

  $form['block_form_forum_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('<b>Enable Forums<b><br /><br />'),
  );

  $form['block_form_forum_active_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('<b>Enable Forum - Active Threads</b>'),
  );

  $form['block_form_forum_active_placement'] = array(
    '#type' => 'select',
    '#default_value' => NULL,
    '#title' => st('Place Forum - Active Threads'),
    '#options' => $options_array,
  );

  $form['new_line_03'] = array(
    '#type' => 'markup',
    '#value' => st('<br />'),
  );

  $form['block_form_forum_new_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('<b>Enable Forum - New Threads</b>'),
  );

  $form['block_form_forum_new_placement'] = array(
    '#type' => 'select',
    '#default_value' => NULL,
    '#title' => st('Place Forum - New Threads'),
    '#options' => $options_array,
  );

  $form['new_line_04'] = array(
    '#type' => 'markup',
    '#value' => st('<br />'),
  );

  $form['block_form_latestnews_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('<b>Enable Latest News</b>'),
  );

  $form['block_form_latestnews_placement'] = array(
    '#type' => 'select',
    '#default_value' => NULL,
    '#title' => st('Place Latest News'),
    '#options' => $options_array,
  );

  $form['new_line_05'] = array(
    '#type' => 'markup',
    '#value' => st('<br />'),
  );

  $form['block_form_menu_primary_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('<b>Enable Menu - Primary Links</b>'),
  );

  $form['block_form_menu_primary_placement'] = array(
    '#type' => 'select',
    '#default_value' => NULL,
    '#title' => st('Place Menu - Primary Links'),
    '#options' => $options_array,
  );

  $form['new_line_06'] = array(
    '#type' => 'markup',
    '#value' => st('<br />'),
  );

  $form['block_form_menu_secondary_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('<b>Enable Menu - Secondary Links</b>'),
  );

  $form['block_form_menu_secondary_placement'] = array(
    '#type' => 'select',
    '#default_value' => NULL,
    '#title' => st('Place Menu - Secondary Links'),
    '#options' => $options_array,
  );

  $form['new_line_07'] = array(
    '#type' => 'markup',
    '#value' => st('<br />'),
  );

  $form['block_form_poll_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('<b>Enable Poll</b>'),
  );

  $form['block_form_poll_placement'] = array(
    '#type' => 'select',
    '#default_value' => NULL,
    '#title' => st('Place Poll'),
    '#options' => $options_array,
  );

  $form['new_line_08'] = array(
    '#type' => 'markup',
    '#value' => st('<br />'),
  );

  //$form['block_form_search_enabled'] = array(
    //'#type' => 'checkbox',
    //'#title' => t('<b>Enable Search</b>'),
  //);

  //$form['block_form_search_placement'] = array(
    //'#type' => 'select',
    //'#default_value' => NULL,
    //'#title' => st('Place Search'),
    //'#options' => $options_array,
  //);

  $form['block_form_shoutbox_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('<b>Enable Shoutbox</b>'),
  );

  $form['block_form_shoutbox_placement'] = array(
    '#type' => 'select',
    '#default_value' => NULL,
    '#title' => st('Place Shoutbox'),
    '#options' => $options_array,
  );

  $form['new_line_09'] = array(
    '#type' => 'markup',
    '#value' => st('<br />'),
  );

  $form['block_form_upcoming_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('<b>Enable Upcoming Events</b>'),
  );

  $form['block_form_upcoming_placement'] = array(
    '#type' => 'select',
    '#default_value' => NULL,
    '#title' => st('Place Upcoming Events'),
    '#options' => $options_array,
  );

  $form['new_line_10'] = array(
    '#type' => 'markup',
    '#value' => st('<br />'),
  );

  $form['block_form_whosnew_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('<b>Enable Whos New</b>'),
  );

  $form['block_form_whosnew_placement'] = array(
    '#type' => 'select',
    '#default_value' => NULL,
    '#title' => st('Place Whos New'),
    '#options' => $options_array,
  );

  $form['new_line_11'] = array(
    '#type' => 'markup',
    '#value' => st('<br />'),
  );

  $form['block_form_whosonline_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('<b>Enable Whos Online</b>'),
  );

  $form['block_form_whosonline_placement'] = array(
    '#type' => 'select',
    '#default_value' => NULL,
    '#title' => st('Place Whos Online'),
    '#options' => $options_array,
  );

  $form['new_line_12'] = array(
    '#type' => 'markup',
    '#value' => st('<br />'),
  );

  $form['block_form_navigation_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('<b>Reposition Navigation Menu</b>'),
  );

  $form['block_form_navigation_placement'] = array(
    '#type' => 'select',
    '#default_value' => NULL,
    '#title' => st('Placement'),
    '#options' => $options_array,
  );

  $form['new_line_13'] = array(
    '#type' => 'markup',
    '#value' => st('<br />'),
  );

  $form['block_form_logon_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('<b>Reposition User Login</b>'),
  );

  $form['block_form_logon_placement'] = array(
    '#type' => 'select',
    '#default_value' => NULL,
    '#title' => st('Placement'),
    '#options' => $options_array,
  );

  // Continue button
  $form['continue'] = array('#type' => 'submit', '#value' => st('Continue'));
  $form['errors'] = array();
  $form['#action'] = $url;
  $form['#redirect'] = FALSE;

  return $form;
}

/*
 * Insert blocks and place them based from the form values
 * Set a variable to signal the blocks form has been submitted
 *
 * @param $task
 *   The current $task of the install system.
 * @param $url
 *   Complete URL to be used for a link or form action
 *
 */

function anytm_block_configuration_form_submit($form, &$form_state) {
  // Set variables from form state
  variable_set('anytm_block_calendar_enabled', $form_state['values']['block_form_calendar_enabled']);
  variable_set('anytm_block_calendar_placement', $form_state['values']['block_form_calendar_placement']);
  variable_set('anytm_block_drupal_footer_enabled', $form_state['values']['block_form_drupal_footer_enabled']);
  variable_set('anytm_block_drupal_footer_placement', $form_state['values']['block_form_drupal_footer_placement']);
  variable_set('anytm_block_forum_enabled', $form_state['values']['block_form_forum_enabled']);
  variable_set('anytm_block_forum_active_enabled', $form_state['values']['block_form_forum_active_enabled']);
  variable_set('anytm_block_forum_active_placement', $form_state['values']['block_form_forum_active_placement']);
  variable_set('anytm_block_forum_new_enabled', $form_state['values']['block_form_forum_new_enabled']);
  variable_set('anytm_block_forum_new_placement', $form_state['values']['block_form_forum_new_placement']);
  variable_set('anytm_block_latestnews_enabled', $form_state['values']['block_form_latestnews_enabled']);
  variable_set('anytm_block_latestnews_placement', $form_state['values']['block_form_latestnews_placement']);
  variable_set('anytm_block_logon_enabled', $form_state['values']['block_form_logon_enabled']);
  variable_set('anytm_block_logon_placement', $form_state['values']['block_form_logon_placement']);
  variable_set('anytm_block_menu_primary_enabled', $form_state['values']['block_form_menu_primary_enabled']);
  variable_set('anytm_block_menu_primary_placement', $form_state['values']['block_form_menu_primary_placement']);
  variable_set('anytm_block_menu_secondary_enabled', $form_state['values']['block_form_menu_secondary_enabled']);
  variable_set('anytm_block_menu_secondary_placement', $form_state['values']['block_form_menu_secondary_placement']);
  variable_set('anytm_block_navigation_enabled', $form_state['values']['block_form_navigation_enabled']);
  variable_set('anytm_block_navigation_placement', $form_state['values']['block_form_navigation_placement']);
  variable_set('anytm_block_poll_enabled', $form_state['values']['block_form_poll_enabled']);
  variable_set('anytm_block_poll_placement', $form_state['values']['block_form_poll_placement']);
  //variable_set('anytm_block_search_enabled', $form_state['values']['block_form_search_enabled']);
  //variable_set('anytm_block_search_placement', $form_state['values']['block_form_search_placement']);
  variable_set('anytm_block_shoutbox_enabled', $form_state['values']['block_form_shoutbox_enabled']);
  variable_set('anytm_block_shoutbox_placement', $form_state['values']['block_form_shoutbox_placement']);
  variable_set('anytm_block_upcoming_enabled', $form_state['values']['block_form_upcoming_enabled']);
  variable_set('anytm_block_upcoming_placement', $form_state['values']['block_form_upcoming_placement']);
  variable_set('anytm_block_whosnew_enabled', $form_state['values']['block_form_whosnew_enabled']);
  variable_set('anytm_block_whosnew_placement', $form_state['values']['block_form_whosnew_placement']);
  variable_set('anytm_block_whosonline_enabled', $form_state['values']['block_form_whosonline_enabled']);
  variable_set('anytm_block_whosonline_placement', $form_state['values']['block_form_whosonline_placement']);
  variable_set('anytm_block_configuration_finished', TRUE);

  // Get theme and a default position to place blocks in
  $theme = variable_get('theme_default', 'garland');
  $placement = variable_get('anytm_placement_array', array());
  $default_cur = current($placement);
  $default_key = array_keys($placement, $default_cur);
  $default_key = $default_key[0];

  // Configre blocks

  db_query("TRUNCATE {blocks}");
  db_query("INSERT INTO {blocks} VALUES (NULL, 'user', 0, '". $theme ."', 1, 0, '". $default_key ."', 0, 0, 0, '', '', 1)");
  db_query("INSERT INTO {blocks} VALUES (NULL, 'user', 1, '". $theme ."', 1, 0, '". $default_key ."', 0, 0, 0, '', '', 1)");

  // If the user ticked enable
  if (variable_get('anytm_block_calendar_enabled', 0) == 1) {
    // Get the placement choice the user made
    $placement_choice = variable_get('anytm_block_calendar_placement', '0');
    // Insert the block configuration
    db_query("INSERT INTO {blocks} VALUES (NULL, 'views', 'calendar-block_1', '". $theme ."', 1, 0, '". $placement_choice ."', 0, 0, 0, '', '', 1)");
  }

  if (variable_get('anytm_block_drupal_footer_enabled', 0) == 1) {
    $placement_choice = variable_get('anytm_block_drupal_footer_placement', '0');
    db_query("UPDATE {blocks} SET region='". $placement_choice ."' WHERE (module = 'system' AND delta = '0')");
  }

  if (variable_get('anytm_block_forum_active_enabled', 0) == 1) {
    $placement_choice = variable_get('anytm_block_forum_active_placement', '0');
    db_query("INSERT INTO {blocks} VALUES (NULL, 'forum', 0, '". $theme ."', 1, 0, '". $placement_choice ."', 0, 0, 0, '', '', 1)");
  }

  if (variable_get('anytm_block_forum_new_enabled', 0) == 1) {
    $placement_choice = variable_get('anytm_block_forum_new_placement', '0');
    db_query("INSERT INTO {blocks} VALUES (NULL, 'forum', 1, '". $theme ."', 1, 0, '". $placement_choice ."', 0, 0, 0, '', '', 1)");
  }

  if (variable_get('anytm_block_latestnews_enabled', 0) == 1) {
    $placement_choice = variable_get('anytm_block_latestnews_placement', '0');
    db_query("INSERT INTO {blocks} VALUES (NULL, 'views', 'latestnews-block_1', '". $theme ."', 1, 0, '". $placement_choice ."', 0, 0, 0, '', '', 1)");
  }

  if (variable_get('anytm_block_logon_enabled', 0) == 1) {
    $placement_choice = variable_get('anytm_block_logon_placement', '0');
    db_query("UPDATE {blocks} SET region='". $placement_choice ."' WHERE (module = 'user' AND delta = '0')");
  }

  if (variable_get('anytm_block_menu_primary_enabled', 0) == 1) {
    $placement_choice = variable_get('anytm_block_menu_primary_placement', '0');
    db_query("INSERT INTO {blocks} VALUES (NULL, 'menu', 'primary-links', '". $theme ."', 1, 0, '". $placement_choice ."', 0, 0, 0, '', '', 1)");
  }

  if (variable_get('anytm_block_menu_secondary_enabled', 0) == 1) {
    $placement_choice = variable_get('anytm_block_menu_secondary_placement', '0');
    db_query("INSERT INTO {blocks} VALUES (NULL, 'menu', 'secondary-links', '". $theme ."', 1, 0, '". $placement_choice ."', 0, 0, 0, '', '', 1)");
  }

  if (variable_get('anytm_block_navigation_enabled', 0) == 1) {
    $placement_choice = variable_get('anytm_block_navigation_placement', '0');
    db_query("UPDATE {blocks} SET region='". $placement_choice ."' WHERE (module = 'user' AND delta = '1')");
  }

  if (variable_get('anytm_block_poll_enabled', 0) == 1) {
    $placement_choice = variable_get('anytm_block_poll_placement', '0');
    db_query("INSERT INTO {blocks} VALUES (NULL, 'poll', 0, '". $theme ."', 1, 0, '". $placement_choice ."', 0, 0, 0, '', '', 1)");
  }

  //if (variable_get('anytm_block_search_enabled', 0) == 1) {
    //$placement_choice = variable_get('anytm_block_search_placement', '0');
    //db_query("INSERT INTO {blocks} VALUES (NULL, 'search', 0, '". $theme ."', 1, 0, '". $placement_choice ."', 0, 0, 0, '', '', 1)");
  //}

  if (variable_get('anytm_block_shoutbox_enabled', 0) == 1) {
    $placement_choice = variable_get('anytm_block_shoutbox_placement', '0');
    db_query("INSERT INTO {blocks} VALUES (NULL, 'shoutbox', 0, '". $theme ."', 1, 0, '". $placement_choice ."', 0, 0, 0, '', '', 1)");
  }

  if (variable_get('anytm_block_upcoming_enabled', 0) == 1) {
    $placement_choice = variable_get('anytm_block_upcoming_placement', '0');
    db_query("INSERT INTO {blocks} VALUES (NULL, 'views', 'calendar-calendar_block_1', '". $theme ."', 1, 0, '". $placement_choice ."', 0, 0, 0, '', '', 1)");
  }

  if (variable_get('anytm_block_whosnew_enabled', 0) == 1) {
    $placement_choice = variable_get('anytm_block_whosnew_placement', '0');
    db_query("INSERT INTO {blocks} VALUES (NULL, 'user', 2, '". $theme ."', 1, 0, '". $placement_choice ."', 0, 0, 0, '', '', 1)");
  }

  if (variable_get('anytm_block_whosonline_enabled', 0) == 1) {
    $placement_choice = variable_get('anytm_block_whosonline_placement', '0');
    db_query("INSERT INTO {blocks} VALUES (NULL, 'user', 3, '". $theme ."', 1, 0, '". $placement_choice ."', 0, 0, 0, '', '', 1)");
  }
}

/*
 * Perform date time zone tasks for this profile.
 *
 * @param $task
 *   The current $task of the install system.
 * @param $url
 *   Complete URL to be used for a link or form action
 *
 * @return
 *   Form for selecting the time zone for the website
 */

function anytm_date_configuration_form(&$form_state, $url) {
  // Setup timezone array
  $date_timezone_array = array(
    'Africa/Accra' => st('Africa/Accra'),
    'Africa/Addis_Ababa' => st('Africa/Addis_Ababa'),
    'Africa/Algiers' => st('Africa/Algiers'),
    'Africa/Asmera' => st('Africa/Asmera'),
    'Africa/Bamako' => st('Africa/Bamako'),
    'Africa/Bangui' => st('Africa/Bangui'),
    'Africa/Banjul' => st('Africa/Banjul'),
    'Africa/Bissau' => st('Africa/Bissau'),
    'Africa/Blantyre' => st('Africa/Blantyre'),
    'Africa/Brazzaville' => st('Africa/Brazzaville'),
    'Africa/Cairo' => st('Africa/Cairo'),
    'Africa/Casablanca' => st('Africa/Casablanca'),
    'Africa/Ceuta' => st('Africa/Ceuta'),
    'Africa/Conakry' => st('Africa/Conakry'),
    'Africa/Dakar' => st('Africa/Dakar'),
    'Africa/Dar_es_Salaam' => st('Africa/Dar_es_Salaam'),
    'Africa/Djibouti' => st('Africa/Djibouti'),
    'Africa/Douala' => st('Africa/Douala'),
    'Africa/El_Aaiun' => st('Africa/El_Aaiun'),
    'Africa/Freetown' => st('Africa/Freetown'),
    'Africa/Gaborone' => st('Africa/Gaborone'),
    'Africa/Harare' => st('Africa/Harare'),
    'Africa/Johannesburg' => st('Africa/Johannesburg'),
    'Africa/Kampala' => st('Africa/Kampala'),
    'Africa/Khartoum' => st('Africa/Khartoum'),
    'Africa/Kigali' => st('Africa/Kigali'),
    'Africa/Lagos' => st('Africa/Lagos'),
    'Africa/Libreville' => st('Africa/Libreville'),
    'Africa/Luanda' => st('Africa/Luanda'),
    'Africa/Lusaka' => st('Africa/Lusaka'),
    'Africa/Malabo' => st('Africa/Malabo'),
    'Africa/Maputo' => st('Africa/Maputo'),
    'Africa/Maseru' => st('Africa/Maseru'),
    'Africa/Mbabane' => st('Africa/Mbabane'),
    'Africa/Mogadishu' => st('Africa/Mogadishu'),
    'Africa/Monrovia' => st('Africa/Monrovia'),
    'Africa/Nairobi' => st('Africa/Nairobi'),
    'Africa/Ndjamena' => st('Africa/Ndjamena'),
    'Africa/Niamey' => st('Africa/Niamey'),
    'Africa/Nouakchott' => st('Africa/Nouakchott'),
    'Africa/Ouagadougou' => st('Africa/Ouagadougou'),
    'Africa/Porto-Novo' => st('Africa/Porto-Novo'),
    'Africa/Sao_Tome' => st('Africa/Sao_Tome'),
    'Africa/Timbuktu' => st('Africa/Timbuktu'),
    'Africa/Tripoli' => st('Africa/Tripoli'),
    'Africa/Tunis' => st('Africa/Tunis'),
    'Africa/Windhoek' => st('Africa/Windhoek'),
    'America/Adak' => st('America/Adak'),
    'America/Anchorage' => st('America/Anchorage'),
    'America/Anguilla' => st('America/Anguilla'),
    'America/Antigua' => st('America/Antigua'),
    'America/Araguaina' => st('America/Araguaina'),
    'America/Argentina/San_Luis' => st('America/Argentina/San_Luis'),
    'America/Aruba' => st('America/Aruba'),
    'America/Asuncion' => st('America/Asuncion'),
    'America/Atka' => st('America/Atka'),
    'America/Barbados' => st('America/Barbados'),
    'America/Belem' => st('America/Belem'),
    'America/Belize' => st('America/Belize'),
    'America/Boa_Vista' => st('America/Boa_Vista'),
    'America/Bogota' => st('America/Bogota'),
    'America/Boise' => st('America/Boise'),
    'America/Buenos_Aires' => st('America/Buenos_Aires'),
    'America/Cambridge_Bay' => st('America/Cambridge_Bay'),
    'America/Cancun' => st('America/Cancun'),
    'America/Caracas' => st('America/Caracas'),
    'America/Catamarca' => st('America/Catamarca'),
    'America/Cayenne' => st('America/Cayenne'),
    'America/Cayman' => st('America/Cayman'),
    'America/Chicago' => st('America/Chicago'),
    'America/Chihuahua' => st('America/Chihuahua'),
    'America/Cordoba' => st('America/Cordoba'),
    'America/Costa_Rica' => st('America/Costa_Rica'),
    'America/Cuiaba' => st('America/Cuiaba'),
    'America/Curacao' => st('America/Curacao'),
    'America/Danmarkshavn' => st('America/Danmarkshavn'),
    'America/Dawson' => st('America/Dawson'),
    'America/Dawson_Creek' => st('America/Dawson_Creek'),
    'America/Denver' => st('America/Denver'),
    'America/Detroit' => st('America/Detroit'),
    'America/Dominica' => st('America/Dominica'),
    'America/Edmonton' => st('America/Edmonton'),
    'America/Eirunepe' => st('America/Eirunepe'),
    'America/El_Salvador' => st('America/El_Salvador'),
    'America/Ensenada' => st('America/Ensenada'),
    'America/Fort_Wayne' => st('America/Fort_Wayne'),
    'America/Fortaleza' => st('America/Fortaleza'),
    'America/Glace_Bay' => st('America/Glace_Bay'),
    'America/Godthab' => st('America/Godthab'),
    'America/Goose_Bay' => st('America/Goose_Bay'),
    'America/Grand_Turk' => st('America/Grand_Turk'),
    'America/Grenada' => st('America/Grenada'),
    'America/Guadeloupe' => st('America/Guadeloupe'),
    'America/Guatemala' => st('America/Guatemala'),
    'America/Guayaquil' => st('America/Guayaquil'),
    'America/Guyana' => st('America/Guyana'),
    'America/Halifax' => st('America/Halifax'),
    'America/Havana' => st('America/Havana'),
    'America/Hermosillo' => st('America/Hermosillo'),
    'America/Indiana/Indianapolis' => st('America/Indiana/Indianapolis'),
    'America/Indiana/Knox' => st('America/Indiana/Knox'),
    'America/Indiana/Marengo' => st('America/Indiana/Marengo'),
    'America/Indiana/Tell_City' => st('America/Indiana/Tell_City'),
    'America/Indiana/Vevay' => st('America/Indiana/Vevay'),
    'America/Indianapolis' => st('America/Indianapolis'),
    'America/Inuvik' => st('America/Inuvik'),
    'America/Iqaluit' => st('America/Iqaluit'),
    'America/Jamaica' => st('America/Jamaica'),
    'America/Jujuy' => st('America/Jujuy'),
    'America/Juneau' => st('America/Juneau'),
    'America/Kentucky/Louisville' => st('America/Kentucky/Louisville'),
    'America/Kentucky/Monticello' => st('America/Kentucky/Monticello'),
    'America/Knox_IN' => st('America/Knox_IN'),
    'America/La_Paz' => st('America/La_Paz'),
    'America/Lima' => st('America/Lima'),
    'America/Los_Angeles' => st('America/Los_Angeles'),
    'America/Louisville' => st('America/Louisville'),
    'America/Maceio' => st('America/Maceio'),
    'America/Managua' => st('America/Managua'),
    'America/Manaus' => st('America/Manaus'),
    'America/Marigot' => st('America/Marigot'),
    'America/Martinique' => st('America/Martinique'),
    'America/Mazatlan' => st('America/Mazatlan'),
    'America/Mendoza' => st('America/Mendoza'),
    'America/Menominee' => st('America/Menominee'),
    'America/Merida' => st('America/Merida'),
    'America/Mexico_City' => st('America/Mexico_City'),
    'America/Miquelon' => st('America/Miquelon'),
    'America/Monterrey' => st('America/Monterrey'),
    'America/Montevideo' => st('America/Montevideo'),
    'America/Montreal' => st('America/Montreal'),
    'America/Montserrat' => st('America/Montserrat'),
    'America/Nassau' => st('America/Nassau'),
    'America/New_York' => st('America/New_York'),
    'America/Nipigon' => st('America/Nipigon'),
    'America/Nome' => st('America/Nome'),
    'America/Noronha' => st('America/Noronha'),
    'America/North_Dakota/Center' => st('America/North_Dakota/Center'),
    'America/Panama' => st('America/Panama'),
    'America/Pangnirtung' => st('America/Pangnirtung'),
    'America/Paramaribo' => st('America/Paramaribo'),
    'America/Phoenix' => st('America/Phoenix'),
    'America/Port-au-Prince' => st('America/Port-au-Prince'),
    'America/Port_of_Spain' => st('America/Port_of_Spain'),
    'America/Porto_Acre' => st('America/Porto_Acre'),
    'America/Porto_Velho' => st('America/Porto_Velho'),
    'America/Puerto_Rico' => st('America/Puerto_Rico'),
    'America/Rainy_River' => st('America/Rainy_River'),
    'America/Rankin_Inlet' => st('America/Rankin_Inlet'),
    'America/Recife' => st('America/Recife'),
    'America/Regina' => st('America/Regina'),
    'America/Rio_Branco' => st('America/Rio_Branco'),
    'America/Rosario' => st('America/Rosario'),
    'America/Santiago' => st('America/Santiago'),
    'America/Santo_Domingo' => st('America/Santo_Domingo'),
    'America/Sao_Paulo' => st('America/Sao_Paulo'),
    'America/Scoresbysund' => st('America/Scoresbysund'),
    'America/Shiprock' => st('America/Shiprock'),
    'America/St_Barthelemy' => st('America/St_Barthelemy'),
    'America/St_Johns' => st('America/St_Johns'),
    'America/St_Kitts' => st('America/St_Kitts'),
    'America/St_Lucia' => st('America/St_Lucia'),
    'America/St_Thomas' => st('America/St_Thomas'),
    'America/St_Vincent' => st('America/St_Vincent'),
    'America/Swift_Current' => st('America/Swift_Current'),
    'America/Tegucigalpa' => st('America/Tegucigalpa'),
    'America/Thule' => st('America/Thule'),
    'America/Thunder_Bay' => st('America/Thunder_Bay'),
    'America/Tijuana' => st('America/Tijuana'),
    'America/Tortola' => st('America/Tortola'),
    'America/Vancouver' => st('America/Vancouver'),
    'America/Virgin' => st('America/Virgin'),
    'America/Whitehorse' => st('America/Whitehorse'),
    'America/Winnipeg' => st('America/Winnipeg'),
    'America/Yakutat' => st('America/Yakutat'),
    'America/Yellowknife' => st('America/Yellowknife'),
    'Antarctica/Casey' => st('Antarctica/Casey'),
    'Antarctica/Davis' => st('Antarctica/Davis'),
    'Antarctica/DumontDUrville' => st('Antarctica/DumontDUrville'),
    'Antarctica/Mawson' => st('Antarctica/Mawson'),
    'Antarctica/McMurdo' => st('Antarctica/McMurdo'),
    'Antarctica/Palmer' => st('Antarctica/Palmer'),
    'Antarctica/South_Pole' => st('Antarctica/South_Pole'),
    'Antarctica/Syowa' => st('Antarctica/Syowa'),
    'Antarctica/Vostok' => st('Antarctica/Vostok'),
    'Arctic/Longyearbyen' => st('Arctic/Longyearbyen'),
    'Asia/Aden' => st('Asia/Aden'),
    'Asia/Almaty' => st('Asia/Almaty'),
    'Asia/Amman' => st('Asia/Amman'),
    'Asia/Anadyr' => st('Asia/Anadyr'),
    'Asia/Aqtau' => st('Asia/Aqtau'),
    'Asia/Aqtobe' => st('Asia/Aqtobe'),
    'Asia/Ashgabat' => st('Asia/Ashgabat'),
    'Asia/Ashkhabad' => st('Asia/Ashkhabad'),
    'Asia/Baghdad' => st('Asia/Baghdad'),
    'Asia/Bahrain' => st('Asia/Bahrain'),
    'Asia/Baku' => st('Asia/Baku'),
    'Asia/Bangkok' => st('Asia/Bangkok'),
    'Asia/Beirut' => st('Asia/Beirut'),
    'Asia/Bishkek' => st('Asia/Bishkek'),
    'Asia/Brunei' => st('Asia/Brunei'),
    'Asia/Calcutta' => st('Asia/Calcutta'),
    'Asia/Choibalsan' => st('Asia/Choibalsan'),
    'Asia/Chongqing' => st('Asia/Chongqing'),
    'Asia/Chungking' => st('Asia/Chungking'),
    'Asia/Colombo' => st('Asia/Colombo'),
    'Asia/Dacca' => st('Asia/Dacca'),
    'Asia/Damascus' => st('Asia/Damascus'),
    'Asia/Dhaka' => st('Asia/Dhaka'),
    'Asia/Dubai' => st('Asia/Dubai'),
    'Asia/Dushanbe' => st('Asia/Dushanbe'),
    'Asia/Gaza' => st('Asia/Gaza'),
    'Asia/Harbin' => st('Asia/Harbin'),
    'Asia/Ho_Chi_Minh' => st('Asia/Ho_Chi_Minh'),
    'Asia/Hong_Kong' => st('Asia/Hong_Kong'),
    'Asia/Hovd' => st('Asia/Hovd'),
    'Asia/Irkutsk' => st('Asia/Irkutsk'),
    'Asia/Istanbul' => st('Asia/Istanbul'),
    'Asia/Jakarta' => st('Asia/Jakarta'),
    'Asia/Jayapura' => st('Asia/Jayapura'),
    'Asia/Jerusalem' => st('Asia/Jerusalem'),
    'Asia/Kabul' => st('Asia/Kabul'),
    'Asia/Kamchatka' => st('Asia/Kamchatka'),
    'Asia/Karachi' => st('Asia/Karachi'),
    'Asia/Kashgar' => st('Asia/Kashgar'),
    'Asia/Katmandu' => st('Asia/Katmandu'),
    'Asia/Kolkata' => st('Asia/Kolkata'),
    'Asia/Krasnoyarsk' => st('Asia/Krasnoyarsk'),
    'Asia/Kuala_Lumpur' => st('Asia/Kuala_Lumpur'),
    'Asia/Kuching' => st('Asia/Kuching'),
    'Asia/Kuwait' => st('Asia/Kuwait'),
    'Asia/Macao' => st('Asia/Macao'),
    'Asia/Magadan' => st('Asia/Magadan'),
    'Asia/Manila' => st('Asia/Manila'),
    'Asia/Muscat' => st('Asia/Muscat'),
    'Asia/Nicosia' => st('Asia/Nicosia'),
    'Asia/Novosibirsk' => st('Asia/Novosibirsk'),
    'Asia/Omsk' => st('Asia/Omsk'),
    'Asia/Phnom_Penh' => st('Asia/Phnom_Penh'),
    'Asia/Pontianak' => st('Asia/Pontianak'),
    'Asia/Pyongyang' => st('Asia/Pyongyang'),
    'Asia/Qatar' => st('Asia/Qatar'),
    'Asia/Rangoon' => st('Asia/Rangoon'),
    'Asia/Riyadh' => st('Asia/Riyadh'),
    'Asia/Saigon' => st('Asia/Saigon'),
    'Asia/Sakhalin' => st('Asia/Sakhalin'),
    'Asia/Seoul' => st('Asia/Seoul'),
    'Asia/Shanghai' => st('Asia/Shanghai'),
    'Asia/Singapore' => st('Asia/Singapore'),
    'Asia/Taipei' => st('Asia/Taipei'),
    'Asia/Tashkent' => st('Asia/Tashkent'),
    'Asia/Tbilisi' => st('Asia/Tbilisi'),
    'Asia/Tel_Aviv' => st('Asia/Tel_Aviv'),
    'Asia/Thimbu' => st('Asia/Thimbu'),
    'Asia/Thimphu' => st('Asia/Thimphu'),
    'Asia/Tokyo' => st('Asia/Tokyo'),
    'Asia/Ujung_Pandang' => st('Asia/Ujung_Pandang'),
    'Asia/Ulaanbaatar' => st('Asia/Ulaanbaatar'),
    'Asia/Ulan_Bator' => st('Asia/Ulan_Bator'),
    'Asia/Urumqi' => st('Asia/Urumqi'),
    'Asia/Vientiane' => st('Asia/Vientiane'),
    'Asia/Vladivostok' => st('Asia/Vladivostok'),
    'Asia/Yakutsk' => st('Asia/Yakutsk'),
    'Asia/Yekaterinburg' => st('Asia/Yekaterinburg'),
    'Asia/Yerevan' => st('Asia/Yerevan'),
    'Atlantic/Azores' => st('Atlantic/Azores'),
    'Atlantic/Bermuda' => st('Atlantic/Bermuda'),
    'Atlantic/Canary' => st('Atlantic/Canary'),
    'Atlantic/Cape_Verde' => st('Atlantic/Cape_Verde'),
    'Atlantic/Faeroe' => st('Atlantic/Faeroe'),
    'Atlantic/Madeira' => st('Atlantic/Madeira'),
    'Atlantic/Reykjavik' => st('Atlantic/Reykjavik'),
    'Atlantic/St_Helena' => st('Atlantic/St_Helena'),
    'Atlantic/Stanley' => st('Atlantic/Stanley'),
    'Australia/ACT' => st('Australia/ACT'),
    'Australia/Adelaide' => st('Australia/Adelaide'),
    'Australia/Brisbane' => st('Australia/Brisbane'),
    'Australia/Broken_Hill' => st('Australia/Broken_Hill'),
    'Australia/Canberra' => st('Australia/Canberra'),
    'Australia/Darwin' => st('Australia/Darwin'),
    'Australia/Hobart' => st('Australia/Hobart'),
    'Australia/LHI' => st('Australia/LHI'),
    'Australia/Lindeman' => st('Australia/Lindeman'),
    'Australia/Lord_Howe' => st('Australia/Lord_Howe'),
    'Australia/Melbourne' => st('Australia/Melbourne'),
    'Australia/North' => st('Australia/North'),
    'Australia/NSW' => st('Australia/NSW'),
    'Australia/Perth' => st('Australia/Perth'),
    'Australia/Queensland' => st('Australia/Queensland'),
    'Australia/South' => st('Australia/South'),
    'Australia/Sydney' => st('Australia/Sydney'),
    'Australia/Tasmania' => st('Australia/Tasmania'),
    'Australia/Victoria' => st('Australia/Victoria'),
    'Australia/West' => st('Australia/West'),
    'Australia/Yancowinna' => st('Australia/Yancowinna'),
    'Europe/Amsterdam' => st('Europe/Amsterdam'),
    'Europe/Andorra' => st('Europe/Andorra'),
    'Europe/Athens' => st('Europe/Athens'),
    'Europe/Belfast' => st('Europe/Belfast'),
    'Europe/Belgrade' => st('Europe/Belgrade'),
    'Europe/Berlin' => st('Europe/Berlin'),
    'Europe/Bratislava' => st('Europe/Bratislava'),
    'Europe/Brussels' => st('Europe/Brussels'),
    'Europe/Bucharest' => st('Europe/Bucharest'),
    'Europe/Budapest' => st('Europe/Budapest'),
    'Europe/Chisinau' => st('Europe/Chisinau'),
    'Europe/Copenhagen' => st('Europe/Copenhagen'),
    'Europe/Dublin' => st('Europe/Dublin'),
    'Europe/Gibraltar' => st('Europe/Gibraltar'),
    'Europe/Helsinki' => st('Europe/Helsinki'),
    'Europe/Istanbul' => st('Europe/Istanbul'),
    'Europe/Kaliningrad' => st('Europe/Kaliningrad'),
    'Europe/Kiev' => st('Europe/Kiev'),
    'Europe/Lisbon' => st('Europe/Lisbon'),
    'Europe/Ljubljana' => st('Europe/Ljubljana'),
    'Europe/London' => st('Europe/London'),
    'Europe/Luxembourg' => st('Europe/Luxembourg'),
    'Europe/Madrid' => st('Europe/Madrid'),
    'Europe/Malta' => st('Europe/Malta'),
    'Europe/Minsk' => st('Europe/Minsk'),
    'Europe/Monaco' => st('Europe/Monaco'),
    'Europe/Moscow' => st('Europe/Moscow'),
    'Europe/Nicosia' => st('Europe/Nicosia'),
    'Europe/Oslo' => st('Europe/Oslo'),
    'Europe/Paris' => st('Europe/Paris'),
    'Europe/Prague' => st('Europe/Prague'),
    'Europe/Riga' => st('Europe/Riga'),
    'Europe/Rome' => st('Europe/Rome'),
    'Europe/Samara' => st('Europe/Samara'),
    'Europe/San_Marino' => st('Europe/San_Marino'),
    'Europe/Sarajevo' => st('Europe/Sarajevo'),
    'Europe/Simferopol' => st('Europe/Simferopol'),
    'Europe/Skopje' => st('Europe/Skopje'),
    'Europe/Sofia' => st('Europe/Sofia'),
    'Europe/Stockholm' => st('Europe/Stockholm'),
    'Europe/Tallinn' => st('Europe/Tallinn'),
    'Europe/Tirane' => st('Europe/Tirane'),
    'Europe/Tiraspol' => st('Europe/Tiraspol'),
    'Europe/Uzhgorod' => st('Europe/Uzhgorod'),
    'Europe/Vaduz' => st('Europe/Vaduz'),
    'Europe/Vatican' => st('Europe/Vatican'),
    'Europe/Vienna' => st('Europe/Vienna'),
    'Europe/Vilnius' => st('Europe/Vilnius'),
    'Europe/Warsaw' => st('Europe/Warsaw'),
    'Europe/Zagreb' => st('Europe/Zagreb'),
    'Europe/Zaporozhye' => st('Europe/Zaporozhye'),
    'Europe/Zurich' => st('Europe/Zurich'),
    'Indian/Antananarivo' => st('Indian/Antananarivo'),
    'Indian/Chagos' => st('Indian/Chagos'),
    'Indian/Comoro' => st('Indian/Comoro'),
    'Indian/Kerguelen' => st('Indian/Kerguelen'),
    'Indian/Mahe' => st('Indian/Mahe'),
    'Indian/Maldives' => st('Indian/Maldives'),
    'Indian/Mauritius' => st('Indian/Mauritius'),
    'Indian/Mayotte' => st('Indian/Mayotte'),
    'Indian/Reunion' => st('Indian/Reunion'),
    'Pacific/Apia' => st('Pacific/Apia'),
    'Pacific/Auckland' => st('Pacific/Auckland'),
    'Pacific/Chatham' => st('Pacific/Chatham'),
    'Pacific/Easter' => st('Pacific/Easter'),
    'Pacific/Efate' => st('Pacific/Efate'),
    'Pacific/Enderbury' => st('Pacific/Enderbury'),
    'Pacific/Fiji' => st('Pacific/Fiji'),
    'Pacific/Galapagos' => st('Pacific/Galapagos'),
    'Pacific/Gambier' => st('Pacific/Gambier'),
    'Pacific/Guadalcanal' => st('Pacific/Guadalcanal'),
    'Pacific/Guam' => st('Pacific/Guam'),
    'Pacific/Honolulu' => st('Pacific/Honolulu'),
    'Pacific/Kiritimati' => st('Pacific/Kiritimati'),
    'Pacific/Kosrae' => st('Pacific/Kosrae'),
    'Pacific/Kwajalein' => st('Pacific/Kwajalein'),
    'Pacific/Majuro' => st('Pacific/Majuro'),
    'Pacific/Marquesas' => st('Pacific/Marquesas'),
    'Pacific/Midway' => st('Pacific/Midway'),
    'Pacific/Nauru' => st('Pacific/Nauru'),
    'Pacific/Niue' => st('Pacific/Niue'),
    'Pacific/Norfolk' => st('Pacific/Norfolk'),
    'Pacific/Noumea' => st('Pacific/Noumea'),
    'Pacific/Pago_Pago' => st('Pacific/Pago_Pago'),
    'Pacific/Pitcairn' => st('Pacific/Pitcairn'),
    'Pacific/Rarotonga' => st('Pacific/Rarotonga'),
    'Pacific/Saipan' => st('Pacific/Saipan'),
    'Pacific/Samoa' => st('Pacific/Samoa'),
    'Pacific/Tahiti' => st('Pacific/Tahiti'),
    'Pacific/Tongatapu' => st('Pacific/Tongatapu'),
    'UTC' => st('UTC'),
  );

  // Setup selection box
  $form['date_timzone_selection'] = array(
    '#type' => 'select',
    '#default_value' => NULL,
    '#title' => st('<p>Select the default site time zone. If in doubt, choose the timezone that is closest to your location which has the same rules for daylight saving time.</p><b>Default time zone</b>'),
    '#options' => $date_timezone_array,
  );

  // Continue button
  $form['continue'] = array('#type' => 'submit', '#value' => st('Continue'));
  $form['errors'] = array();
  $form['#action'] = $url;
  $form['#redirect'] = FALSE;

  return $form;
}

/*
 * Insert timezone from the form values
 * Set a variable to signal the timezone form has been submitted
 *
 * @param $task
 *   The current $task of the install system.
 * @param $url
 *   Complete URL to be used for a link or form action
 *
 */

function anytm_date_configuration_form_submit($form, &$form_state) {
  // Set default timezone variable
  variable_set('date_default_timezone_name', $form_state['values']['date_timzone_selection']);
  variable_set('anytm_date_configuration_finished', TRUE);
}

/*
 * Perform user setup 1 task for this profile.
 * Add user permissions & roles
 *
 * @param $task
 *   The current $task of the install system.
 * @param $url
 *   Complete URL to be used for a link or form action
 *
 * @return
 *   Form asking whether the user want to insert users
 */

function anytm_user_setup1_form(&$form_state, $url) {
  // Remove anonymous user permissions
  $rid = 1;
  $permissions = array('access content');
  install_remove_permissions($rid, $permissions);

  // Add admin role
  $rid = install_add_role('admin');

  // Setup admin permissions
  $permissions = array('view advanced help index', 'view advanced help popup', 'view advanced help topic', 'administer blocks',
    'use PHP for block visibility', 'access comments', 'administer comments', 'post comments', 'post comments without approval',
    'edit field_datetime', 'edit field_location', 'view field_datetime', 'view field_location', 'view date repeats',
    'administer date_php4 settings', 'administer date tools', 'administer filters', 'administer forums', 'create forum topics',
    'delete any forum topic', 'delete own forum topics', 'edit any forum topic', 'edit own forum topics', 'administer languages',
    'translate interface', 'administer menu', 'access content', 'administer content types', 'administer nodes',
    'create calendarevent content', 'create page content', 'delete any calendarevent content', 'delete any page content',
    'delete own calendarevent content', 'delete own page content', 'delete revisions', 'edit any calendarevent content',
    'edit any page content', 'edit own calendarevent content', 'edit own page content', 'revert revisions', 'view revisions',
    'cancel own vote', 'create poll content', 'delete any poll content', 'delete own poll content', 'edit any poll content',
    'edit own poll content', 'inspect all votes', 'vote on polls', 'administer shoutbox', 'delete own shouts', 'edit own shouts',
    'moderate shoutbox', 'post shouts', 'post shouts without approval', 'access administration pages', 'access site reports',
    'administer actions', 'administer files', 'administer site configuration', 'select different theme', 'administer taxonomy',
    'access user profiles', 'administer permissions', 'administer users', 'change own username', 'access all views',
    'administer views',
  );
  // Add admin permissions
  install_add_permissions($rid, $permissions);

  // Add manager role
  $rid = install_add_role('manager');

  // Setup manager permissions
  $permissions = array('view advanced help index', 'view advanced help popup', 'view advanced help topic', 'administer blocks',
    'access comments', 'administer comments', 'post comments', 'post comments without approval', 'edit field_datetime',
    'edit field_location', 'view field_datetime', 'view field_location', 'view date repeats', 'administer forums',
    'create forum topics', 'delete any forum topic', 'delete own forum topics', 'edit any forum topic', 'edit own forum topics',
    'translate interface', 'access content', 'create calendarevent content', 'create page content',
    'delete any calendarevent content', 'delete any page content', 'delete own calendarevent content', 'delete own page content',
    'delete revisions', 'edit any calendarevent content', 'edit any page content', 'edit own calendarevent content',
    'edit own page content', 'revert revisions', 'view revisions', 'cancel own vote', 'create poll content',
    'delete any poll content', 'delete own poll content', 'edit any poll content', 'edit own poll content', 'inspect all votes',
    'vote on polls', 'administer shoutbox', 'delete own shouts', 'edit own shouts', 'moderate shoutbox', 'post shouts',
    'post shouts without approval', 'access site reports', 'select different theme', 'access user profiles',
    'administer permissions', 'administer users', 'change own username', 'access all views', 'administer views',
  );

  // Add manager permissions
  install_add_permissions($rid, $permissions);

  // Add member role
  $rid = install_add_role('member');

  // Setup member permissions
  $permissions = array('view advanced help index', 'view advanced help popup', 'view advanced help topic', 'access comments',
    'post comments', 'create forum topics', 'translate interface', 'access content', 'inspect all votes', 'vote on polls',
    'post shouts', 'post shouts without approval', 'access user profiles', 'access all views',
  );

  // Add member permissions
  install_add_permissions($rid, $permissions);

  // User question
  $users_question = st('<p>Do you want to add some users? You will need to give them a username and email.');
  $users_question .= st(' And their password will be \'password\' until it is changed.</p>');

  $form['users'] = array(
    '#type' => 'markup',
    '#value' => $users_question,
  );

  // User options
  $form['user_setup_do'] = array(
    '#type' => 'radios',
    '#title' => t('Add Users'),
    '#options' => array(t('No, I\'ll add some later.'), t('Yes, I\'ll add some now.')),
  );

  // Continue button
  $form['continue'] = array('#type' => 'submit', '#value' => st('Continue'));
  $form['errors'] = array();
  $form['#action'] = $url;
  $form['#redirect'] = FALSE;

  return $form;
}

/*
 * Set a variable to signal the wether the user wants to add more users
 *
 * @param $task
 *   The current $task of the install system.
 * @param $url
 *   Complete URL to be used for a link or form action
 *
 */

function anytm_user_setup1_form_submit($form, &$form_state) {
  $do = $form_state['values']['user_setup_do'];
  variable_set('anytm_setup_users', $do); // Set wether the user wants to add more users variable
  variable_set('anytm_user_setup1_finished', TRUE); // Set form finished variable
}

/*
 * Perform user setup 2 tasks for this profile.
 *
 * @param $task
 *   The current $task of the install system.
 * @param $url
 *   Complete URL to be used for a link or form action
 *
 * @return
 *   Form asking for user details and roles
 */

function anytm_user_setup2_form(&$form_state, $url) {
  // Array of user you have added
  $user_array_exists = variable_get('anytm_user_array_exists', array());

  // Display list of users added
  if ($user_array_exists != FALSE) {
    $form['users_01'] = array(
      '#type' => 'markup',
      '#value' => st('<p><b>You have added user(s):</b>'),
    );

    $user_array = variable_get('anytm_user_setup_array', array());

    $users = st('<br />');
    foreach ($user_array as $username) {
      $users .= $username;
      $users .= st('<br />');
    }

    $form['users_02'] = array(
      '#type' => 'markup',
      '#value' => $users,
    );

    $form['users_03'] = array(
      '#type' => 'markup',
      '#value' => st('</p>'),
    );
  }

  // User setup form

  $form['user_setup_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#default_value' => $empty,
    '#required' => TRUE,
  );

  $form['user_setup_email'] = array(
    '#type' => 'textfield',
    '#title' => t('User Email'),
    '#default_value' => $empty,
    '#required' => TRUE,
  );

  $user_array = array(
    'admin' => st('Admin'),
    'manager' => st('Team Manager'),
    'member' => st('Team Member'),
  );

  $form['user_setup_role'] = array(
    '#type' => 'select',
    '#default_value' => NULL,
    '#title' => st('<p>Select the users role'),
    '#options' => $user_array,
  );

  $form['user_setup_finished'] = array(
    '#type' => 'radios',
    '#title' => t('Finished adding users?'),
    '#options' => array(t('No, Add More.'), t('Yes, I\'m Done')),
    '#required' => TRUE,
  );

  $form['users_note'] = array(
      '#type' => 'markup',
      '#value' => st('<p><b>Note: User passwords will be \'password\' until it is changed</b></p>'),
  );

  // Continue button
  $form['continue'] = array('#type' => 'submit', '#value' => st('Continue'));
  $form['errors'] = array();
  $form['#action'] = $url;
  $form['#redirect'] = FALSE;

  return $form;
}

/*
 * Insert users with values from form
 * Set a variable to signal the users have been and either finished or will ad another submitted
 *
 * @param $task
 *   The current $task of the install system.
 * @param $url
 *   Complete URL to be used for a link or form action
 *
 */

function anytm_user_setup2_form_submit($form, &$form_state) {
  // Get values from form
  $username = $form_state['values']['user_setup_name'];
  $form_state['values']['user_setup_name'] = '';
  $email = $form_state['values']['user_setup_email'];
  $form_state['values']['user_setup_email'] == '';
  $roles = array($form_state['values']['user_setup_role']);
  $form_state['values']['user_setup_role'] = '';
  $finished = $form_state['values']['user_setup_finished'];

  // Add user
  install_add_user($username, 'password', $email, $roles);

  if (variable_get('anytm_user_array_exists', FALSE) == FALSE) {
    $user_array = array();
    variable_set('anytm_user_array_exists', TRUE);
  }
  else {
    $user_array = variable_get('anytm_user_setup_array', $user_array);
  }

  $user_array[] = $username;
  variable_set('anytm_user_setup_array', $user_array); // Add user to array

  if ($finished) {
    variable_set('anytm_user_setup2_finished', TRUE); // Set form finished variable
  }
}

/*
 * Perform shoutbox configuration task for this profile.
 *
 * @param $task
 *   The current $task of the install system.
 * @param $url
 *   Complete URL to be used for a link or form action
 *
 * @return
 *   Form asking for a first shout
 */

function anytm_shoutbox_form(&$form_state, $url) {
  // Shoutbox form
  $shoutbox_info = st('<p>A shoutbox is chat module, where users can post comments for other');
  $shoutbox_info .= st(' users to see in real time. You can add your first shout below.</p>');

  $form['forum_info'] = array(
        '#type' => 'markup',
        '#value' => $shoutbox_info,
  );

  $form['shout_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Shout User'),
    '#default_value' => t('AnyTM'),
    '#required' => TRUE,
  );

  $form['shout_message'] = array(
    '#type' => 'textarea',
    '#title' => t('Shout Message'),
    '#default_value' => t('Welcome to your AnyTM - Any Team Management System Website!'),
    '#required' => TRUE,
  );

  // Continue button
  $form['continue'] = array(
    '#type' => 'submit',
    '#value' => st('Continue'),
  );

  $form['errors'] = array();
  $form['#action'] = $url;
  $form['#redirect'] = FALSE;

  return $form;
}

/*
 * Insert a shoutbox with a shout from the form values
 * Set a variable to signal the shoutbox form has been submitted
 *
 * @param $task
 *   The current $task of the install system.
 * @param $url
 *   Complete URL to be used for a link or form action
 *
 */

function anytm_shoutbox_form_submit($form, &$form_state) {
  // Gets values from form
  $name = $form_state['values']['shout_name'];
  $message = $form_state['values']['shout_message'];
  $url = $form_state['values']['shout_url'];

  // Insert shout
  db_query("INSERT INTO {shoutbox} VALUES ('', 0, '%s', '%s', '', 0, 0, 0, '', '')", $name, $message);

  variable_set('anytm_shoutbox_finished', TRUE); // Set form finished variable
}

/*
 * Perform poll configuration task for this profile.
 *
 * @param $task
 *   The current $task of the install system.
 * @param $url
 *   Complete URL to be used for a link or form action
 *
 * @return
 *   Form asking for a poll for users
 */

function anytm_poll_form(&$form_state, $url) {
  // Poll form
  $poll_info = st('<p>A poll is a question with a set of possible responses. A poll, once created,');
  $poll_info .= st(' automatically provides a simple running count of the number of votes received');
  $poll_info .= st(' for each response.</p>');

  $form['forum_info'] = array(
        '#type' => 'markup',
        '#value' => $poll_info,
  );

  $form['poll_question'] = array(
    '#type' => 'textfield',
    '#title' => t('Poll Question'),
    '#default_value' => t('Do you like our new team website?'),
    '#required' => TRUE,
  );

  $form['poll_answer1'] = array(
    '#type' => 'textfield',
    '#title' => t('Answer 1'),
    '#default_value' => t('Yes'),
    '#required' => TRUE,
  );

  $form['poll_answer2'] = array(
    '#type' => 'textfield',
    '#title' => t('Answer 2'),
    '#default_value' => t('No'),
    '#required' => TRUE,
  );

  // Continue button
  $form['continue'] = array(
    '#type' => 'submit',
    '#value' => st('Continue'),
  );

  $form['errors'] = array();
  $form['#action'] = $url;
  $form['#redirect'] = FALSE;

  return $form;
}

/*
 * Insert a poll with values from forms
 * Set a variable to signal the poll form has been submitted
 *
 * @param $task
 *   The current $task of the install system.
 * @param $url
 *   Complete URL to be used for a link or form action
 *
 */

function anytm_poll_form_submit($form, &$form_state) {
  // Get values from form
  $question = $form_state['values']['poll_question'];
  $answer1 = $form_state['values']['poll_answer1'];
  $answer2 = $form_state['values']['poll_answer2'];

  $node = array(
    //'vid' => 1,
    'type' => 'poll',
    'title' => $question,
    'status' => 1,
    'promote' => 1,
    'comment' => 2,
    'runtime' => 0,
    'active' => 1,
    'choice' => array(
      array(
        'chtext' => $answer1,
        'chvotes' => 0,
        'chorder' => 0,
      ),
      array(
        'chtext' => $answer2,
        'chvotes' => 0,
        'chorder' => 1,
      )
    )
  );

  // Save poll
  if ($node = node_submit($node)) {
    node_save($node);
  }

  variable_set('anytm_poll_finished', TRUE); // Set form finished variable
}

/*
 * Perform forum configuration task for this profile.
 *
 * @param $task
 *   The current $task of the install system.
 * @param $url
 *   Complete URL to be used for a link or form action
 *
 * @return
 *   Form asking for several forum containers for the team site
 */

function anytm_forum_form(&$form_state, $url) {
  // Forum form
  $forum_info = st('<p>Containers hold forums, and forums hold forum topics (a forum topic ');
  $forum_info .= st('is the initial post to a threaded discussion). Forum topics and forum ');
  $forum_info .= st('and forum posts can be added later once your installation is complete.</p>');

  $form['forum_info'] = array(
        '#type' => 'markup',
        '#value' => $forum_info,
  );

  $form['forum_container1'] = array(
    '#type' => 'textfield',
    '#title' => t('Fourm Container 1'),
    '#default_value' => t('Latest News'),
  );

  $form['forum_container2'] = array(
    '#type' => 'textfield',
    '#title' => t('Fourm Container 2'),
    '#default_value' => t('Upcoming Events'),
  );

  $form['forum_container3'] = array(
    '#type' => 'textfield',
    '#title' => t('Fourm Container 3'),
    '#default_value' => t('Changes To Procedure'),
  );

  $form['forum_container4'] = array(
    '#type' => 'textfield',
    '#title' => t('Fourm Container 4'),
    '#default_value' => t('Help & Tutorials '),
  );

  $form['forum_container5'] = array(
    '#type' => 'textfield',
    '#title' => t('Fourm Container 5'),
    '#default_value' => t(''),
  );

  // Continue button
  $form['continue'] = array(
    '#type' => 'submit',
    '#value' => st('Fourm Continue'),
  );

  $form['errors'] = array();
  $form['#action'] = $url;
  $form['#redirect'] = FALSE;

  return $form;
}

/*
 * Insert forum containers with values from forms
 * Set a variable to signal the forum form has been submitted
 *
 * @param $task
 *   The current $task of the install system.
 * @param $url
 *   Complete URL to be used for a link or form action
 *
 */

function anytm_forum_form_submit($form, &$form_state) {
  // Get form values
  $container1 = $form_state['values']['forum_container1'];
  $container2 = $form_state['values']['forum_container2'];
  $container3 = $form_state['values']['forum_container3'];
  $container4 = $form_state['values']['forum_container4'];
  $container5 = $form_state['values']['forum_container5'];

  $forum_containers = array();

  // Insert forum containers

  if ($container1 != '') {
    db_query("INSERT INTO {term_data} VALUES (NULL, 1, '%s', NULL, 0)", $container1);
    $object1 = db_query("SELECT tid FROM {term_data} WHERE name='%s'", $container1);
    $tid1 = db_fetch_object($object1);
    $tid1 = $tid1->tid;
    $tid1 = intval($tid1);
    db_query("INSERT INTO {term_hierarchy} VALUES (%d, 0)", $tid1);
    $forum_containers[] = $tid1;
  }

  if ($container2 != '') {
    db_query("INSERT INTO {term_data} VALUES (NULL, 1, '%s', NULL, 0)", $container2);
    $object2 = db_query("SELECT tid FROM {term_data} WHERE name='%s'", $container2);
    $tid2 = db_fetch_object($object2);
    $tid2 = $tid2->tid;
    $tid2 = intval($tid2);
    db_query("INSERT INTO {term_hierarchy} VALUES (%d, 0)", $tid2);
    $forum_containers[] = $tid2;
  }

  if ($container3 != '') {
    db_query("INSERT INTO {term_data} VALUES (NULL, 1, '%s', NULL, 0)", $container3);
    $object3 = db_query("SELECT tid FROM {term_data} WHERE name='%s'", $container3);
    $tid3 = db_fetch_object($object3);
    $tid3 = $tid3->tid;
    $tid3 = intval($tid3);
    db_query("INSERT INTO {term_hierarchy} VALUES (%d, 0)", $tid3);
    $forum_containers[] = $tid3;
  }

  if ($container4 != '') {
    db_query("INSERT INTO {term_data} VALUES (NULL, 1, '%s', NULL, 0)", $container4);
    $object4 = db_query("SELECT tid FROM {term_data} WHERE name='%s'", $container4);
    $tid4 = db_fetch_object($object4);
    $tid4 = $tid4->tid;
    $tid4 = intval($tid4);
    db_query("INSERT INTO {term_hierarchy} VALUES (%d, 0)", $tid4);
    $forum_containers[] = $tid4;
  }

  if ($container5 != '') {
    db_query("INSERT INTO {term_data} VALUES (NULL, 1, '%s', NULL, 0)", $container5);
    $object5 = db_query("SELECT tid FROM {term_data} WHERE name='%s'", $container5);
    variable_set('object5', $object5);
    $tid5 = db_fetch_object($object5);
    $tid5 = $tid5->tid;
    $tid5 = intval($tid5);
    db_query("INSERT INTO {term_hierarchy} VALUES (%d, 0)", $tid5);
    $forum_containers[] = $tid5;
  }

  variable_set('forum_containers', $forum_containers);  // Set forum contianers variable
  variable_set('anytm_forum_finished', TRUE); // Set form finished variable
}

/*
 * Perform page inserts for this profile.
 *
 * @param $task
 *   The current $task of the install system.
 * @param $url
 *   Complete URL to be used for a link or form action
 *
 * @return
 *   Form asking for page titles and bodies
 */

function anytm_frontpage_form(&$form_state, $url) {
  // Page form
  $page_info = st('<p>A page, is a simple method for creating and displaying information that');
  $page_info .= st(' rarely changes, such as an "About us" section of a website. By default, ');
  $page_info .= st('a page entry does not allow visitor comments. A page is not commonly ');
  $page_info .= st('featured as a site\'s initial home page, but you can set it as one if ');
  $page_info .= st('you wish.</p>');

  $form['page_intro'] = array(
    '#type' => 'markup',
    '#value' => $page_info,
  );

  $form['page_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Page Title'),
    '#default_value' => t('Home'),
    '#required' => TRUE,
  );

  $form['page_contents'] = array(
    '#type' => 'textarea',
    '#title' => t('Page Contents'),
    '#default_value' => t('Welcome to your AnyTM Team Management Website. This is your home page.'),
    '#required' => TRUE,
  );

  $form['page_home'] = array(
    '#type' => 'checkbox',
    '#title' => t('Set this as home page.'),
  );

  $form['page_setup_finished'] = array(
    '#type' => 'radios',
    '#title' => t('Finished adding pages?'),
    '#options' => array(t('No, Add More.'), t('Yes, I\'m Done')),
    '#required' => TRUE,
  );

  // Continue button
  $form['continue'] = array(
    '#type' => 'submit',
    '#value' => st('Continue'),
  );

  $form['errors'] = array();
  $form['#action'] = $url;
  $form['#redirect'] = FALSE;

  return $form;
}

/*
 * Insert pages with values from forms
 * Set a variable to signal the pages form has been submitted
 *
 * @param $task
 *   The current $task of the install system.
 * @param $url
 *   Complete URL to be used for a link or form action
 *
 */

function anytm_frontpage_form_submit($form, &$form_state) {
  // Get form values
  $page_title = $form_state['values']['page_title'];
  $page_contents = $form_state['values']['page_contents'];
  $finished = $form_state['values']['page_setup_finished'];
  $home_page = $form_state['values']['page_home'];

  // Create node
  $node = array(
    'type' => 'page',
    'title' => $page_title,
    'body' => $page_contents,
    'status' => 1,
    'promote' => 1,
  );

  // Save node
  if ($node = node_submit($node)) {
    node_save($node);
  }

  // Get node ID
  $nid = $node->nid;

  // Create values for menu link
  $nodepath = "node/";
  $nodepath .= strval($nid);
  $noderoute = "node/%";

  $array = array(
    'attributes' => array(
      'title' => $page_title,
    ),
  );

  $array = serialize($array);

  // Insert menu link
  db_query("INSERT INTO {menu_links} VALUES ('primary-links', NULL, 0,
    '%s', '%s', '%s', '%s', 'menu', 0, 0, 0, 0, -1, 1, 1, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0)", $nodepath, $noderoute, $page_title, $array
  );

  if ($home_page) {
    variable_set('site_frontpage', $nodepath); // Set as homepage
  }

  if ($finished) {
    variable_set('anytm_frontpage_finished', TRUE); //Set form as finished
  }
}

/*
 * Perform final infromation for this profile.
 *
 * @param $task
 *   The current $task of the install system.
 * @param $url
 *   Complete URL to be used for a link or form action
 *
 * @return
 *   Form explaining final steps for install
 */

function anytm_final_form(&$form_state, $url) {
  // Final form
  $final_info = st('<p>You have almost finised. When you open your new website, you may want to ');
  $final_info .= st('administer some things yourself. When you go to the adminsiter page you will ');
  $final_info .= st('get a message saying:<br /><b>"Cron has not run. Please visit the status report');
  $final_info .= st(' for more information."</b><br />Cron is an important feature of Drupal that ');
  $final_info .= st('needs to run periodically. So be sure to set it up once you have your site running.</p>');

  $form['final_info'] = array(
    '#type' => 'markup',
    '#value' => $final_info,
  );

  // Continue button
  $form['continue'] = array('#type' => 'submit', '#value' => st('Continue'));
  $form['errors'] = array();
  $form['#action'] = $url;
  $form['#redirect'] = FALSE;

  return $form;
}

/*
 * Set a variable to signal the final form has been submitted
 *
 * @param $task
 *   The current $task of the install system.
 * @param $url
 *   Complete URL to be used for a link or form action
 *
 */

function anytm_final_form_submit($form, &$form_state) {
  variable_set('anytm_final_finished', TRUE); // Set form finished value
}

/*
 * Create custom fileds used by the AnyTM installation profile
 *   Calendar Event Node and fields added include:
 *     Location
 *     Date Start - Finish
 */

function anytm_include_calendar_field() {
  $content = array();

  ob_start();

  $content['type']  = array(
    'name' => 'Calender Event',
    'type' => 'calendarevent',
    'description' => 'This can be anything from an employees shift, to your teams soccer game.',
    'title_label' => 'Title',
    'body_label' => 'Notes',
    'min_word_count' => '0',
    'help' => '',
    'node_options' =>
    array(
      'status' => TRUE,
      'promote' => TRUE,
      'sticky' => FALSE,
      'revision' => FALSE,
    ),
    'old_type' => 'calendarevent',
    'orig_type' => '',
    'module' => 'node',
    'custom' => '1',
    'modified' => '1',
    'locked' => '0',
  );
  $content['fields']  = array(
    0 =>
    array(
      'label' => 'Location',
      'field_name' => 'field_location',
      'type' => 'text',
      'widget_type' => 'text_textfield',
      'change' => 'Change basic information',
      'weight' => '-4',
      'rows' => 5,
      'size' => '60',
      'description' => 'The location of the event.',
      'default_value' =>
      array(
        0 =>
        array(
          'value' => '',
          '_error_element' => 'default_value_widget][field_location][0][value',
        ),
      ),
      'default_value_php' => '',
      'default_value_widget' => NULL,
      'required' => 1,
      'multiple' => '0',
      'text_processing' => '0',
      'max_length' => '',
      'allowed_values' => '',
      'allowed_values_php' => '',
      'op' => 'Save field settings',
      'module' => 'text',
      'widget_module' => 'text',
      'columns' =>
      array(
        'value' =>
        array(
          'type' => 'text',
          'size' => 'big',
          'not null' => FALSE,
          'sortable' => TRUE,
          'views' => TRUE,
        ),
      ),
      'display_settings' =>
      array(
        'label' =>
        array(
          'format' => 'above',
          'exclude' => 0,
        ),
        'teaser' =>
        array(
          'format' => 'default',
          'exclude' => 0,
        ),
        'full' =>
        array(
          'format' => 'default',
          'exclude' => 0,
        ),
        4 =>
        array(
          'format' => 'default',
          'exclude' => 0,
        ),
      ),
    ),
    1 =>
    array(
      'label' => 'Date & Time',
      'field_name' => 'field_datetime',
      'type' => 'datetime',
      'widget_type' => 'date_popup_repeat',
      'change' => 'Change basic information',
      'weight' => '-3',
      'default_value' => 'now',
      'default_value2' => 'same',
      'default_value_code' => '',
      'default_value_code2' => '',
      'input_format' => 'Y-m-d H:i:s',
      'input_format_custom' => '',
      'year_range' => '-3:+5',
      'increment' => '1',
      'advanced' =>
      array(
        'label_position' => 'above',
        'text_parts' =>
        array(
          'year' => 0,
          'month' => 0,
          'day' => 0,
          'hour' => 0,
          'minute' => 0,
          'second' => 0,
        ),
      ),
      'label_position' => 'above',
      'text_parts' =>
      array(
      ),
      'description' => 'The start and finish day and time of your event.',
      'required' => 1,
      'multiple' => 1,
      'repeat' => 1,
      'todate' => 'optional',
      'granularity' =>
      array(
        'year' => 'year',
        'month' => 'month',
        'day' => 'day',
        'hour' => 'hour',
        'minute' => 'minute',
      ),
      'default_format' => 'medium',
      'tz_handling' => 'site',
      'timezone_db' => 'UTC',
      'repeat_collapsed' => '1',
      'op' => 'Save field settings',
      'module' => 'date',
      'widget_module' => 'date',
      'columns' =>
      array(
        'value' =>
        array(
          'type' => 'datetime',
          'not null' => FALSE,
          'sortable' => TRUE,
          'views' => TRUE,
        ),
        'value2' =>
        array(
          'type' => 'datetime',
          'not null' => FALSE,
          'sortable' => TRUE,
          'views' => FALSE,
        ),
        'rrule' =>
        array(
          'type' => 'text',
          'not null' => FALSE,
          'sortable' => FALSE,
          'views' => FALSE,
        ),
      ),
      'display_settings' =>
      array(
        'label' =>
        array(
          'format' => 'above',
          'exclude' => 0,
        ),
        'teaser' =>
        array(
          'format' => 'default',
          'exclude' => 0,
        ),
        'full' =>
        array(
          'format' => 'default',
          'exclude' => 0,
        ),
        4 =>
        array(
          'format' => 'default',
          'exclude' => 0,
        ),
      ),
    ),
  );
  $content['extra']  = array(
    'title' => '-5',
    'body_field' => '-2',
    'revision_information' => '-1',
  );
  ob_end_clean();

  $form_state = array();
  $form = content_copy_import_form($form_state, $type_name);

  $form_state['values']['type_name'] = $type_name ? $type_name : '<create>';
  $form_state['values']['macro'] = '$content = '. var_export($content, 1) .';';
  $form_state['values']['op'] = t('Import');

  content_copy_import_form_submit($form, $form_state);
}

/*
 * Include an updated view used by the AnyTM installation profile
 *   Calander is a block that shows any calendar entry type
 *   nodes, this has been updated from the default calendar
 *   view to use AnyTMs custom fields
 */

function anytm_include_calendar_view() {
  views_include('view');

  $view = new view;
  $view->name = 'calendar';
  $view->description = 'A multi-dimensional calendar view with back/next navigation.';
  $view->tag = 'Calendar';
  $view->view_php = '';
  $view->base_table = 'node';
  $view->is_cacheable = FALSE;
  $view->api_version = 2;
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->override_option('fields', array(
    'title' => array(
      'label' => '',
      'link_to_node' => 1,
      'exclude' => 0,
      'id' => 'title',
      'field' => 'title',
      'table' => 'node',
      'relationship' => 'none',
    ),
    'changed' => array(
      'label' => '',
      'link_to_node' => 0,
      'exclude' => 0,
      'id' => 'changed',
      'field' => 'changed',
      'table' => 'node',
      'relationship' => 'none',
      'date_format' => 'small',
    ),
  ));
  $handler->override_option('sorts', array(
    'changed' => array(
      'order' => 'ASC',
      'delta' => '-1',
      'id' => 'changed',
      'table' => 'node',
      'field' => 'changed',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('arguments', array(
    'date_argument' => array(
      'default_action' => 'default',
      'style_plugin' => 'default_summary',
      'style_options' => array(),
      'wildcard' => 'all',
      'wildcard_substitution' => 'All',
      'title' => '',
      'default_argument_type' => 'date',
      'default_argument' => '',
      'validate_type' => 'none',
      'validate_fail' => 'not found',
      'date_fields' => array(
        'node.changed' => 'node.changed',
      ),
      'year_range' => '-3:+3',
      'date_method' => 'OR',
      'granularity' => 'month',
      'id' => 'date_argument',
      'table' => 'node',
      'field' => 'date_argument',
      'relationship' => 'none',
      'default_argument_user' => 0,
      'default_argument_fixed' => '',
      'default_argument_php' => '',
      'validate_argument_node_type' => array(),
      'validate_argument_node_access' => 0,
      'validate_argument_nid_type' => 'nid',
      'validate_argument_vocabulary' => array(),
      'validate_argument_type' => 'tid',
      'validate_argument_php' => '',
      'override' => array(
        'button' => 'Override',
      ),
      'default_options_div_prefix' => '',
    ),
  ));
  $handler->override_option('filters', array(
    'type' => array(
      'operator' => 'in',
      'value' => array(
        'calendarevent' => 'calendarevent',
      ),
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'type',
      'table' => 'node',
      'field' => 'type',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'none',
    'role' => array(),
    'perm' => '',
  ));
  $handler->override_option('cache', array(
    'type' => 'none',
  ));
  $handler->override_option('title', 'Calendar');
  $handler->override_option('header_empty', 1);
  $handler->override_option('items_per_page', 0);
  $handler->override_option('use_more', 0);
  $handler->override_option('style_plugin', 'calendar_nav');
  $handler = $view->new_display('calendar', 'Calendar page', 'calendar_1');
  $handler->override_option('path', 'calendar');
  $handler->override_option('menu', array(
    'type' => 'none',
    'title' => '',
    'description' => '',
    'weight' => 0,
    'name' => 'navigation',
  ));
  $handler->override_option('tab_options', array(
    'type' => 'none',
    'title' => '',
    'description' => '',
    'weight' => 0,
  ));
  $handler->override_option('calendar_colors', array(
    '0' => array(),
  ));
  $handler->override_option('calendar_colors_vocabulary', array());
  $handler->override_option('calendar_colors_taxonomy', array());
  $handler->override_option('calendar_popup', 0);
  $handler->override_option('calendar_date_link', '');
  $handler = $view->new_display('calendar_block', 'Calendar block', 'calendar_block_1');
  $handler->override_option('block_description', 'Calendar');
  $handler->override_option('block_caching', -1);
  $handler = $view->new_display('calendar_period', 'Year view', 'calendar_period_1');
  $handler->override_option('style_plugin', 'calendar_style');
  $handler->override_option('style_options', array(
    'display_type' => 'year',
    'name_size' => 1,
    'max_items' => 0,
  ));
  $handler->override_option('attachment_position', 'after');
  $handler->override_option('inherit_arguments', TRUE);
  $handler->override_option('inherit_exposed_filters', TRUE);
  $handler->override_option('displays', array(
    'calendar_1' => 'calendar_1',
    'default' => 0,
    'calendar_block_1' => 0,
  ));
  $handler->override_option('calendar_type', 'year');
  $handler = $view->new_display('calendar_period', 'Month view', 'calendar_period_2');
  $handler->override_option('style_plugin', 'calendar_style');
  $handler->override_option('style_options', array(
    'display_type' => 'month',
    'name_size' => '99',
    'with_weekno' => '1',
    'date_fields' => NULL,
    'max_items' => 0,
  ));
  $handler->override_option('attachment_position', 'after');
  $handler->override_option('inherit_arguments', TRUE);
  $handler->override_option('inherit_exposed_filters', TRUE);
  $handler->override_option('displays', array(
    'calendar_1' => 'calendar_1',
    'default' => 0,
    'calendar_block_1' => 0,
  ));
  $handler->override_option('calendar_type', 'month');
  $handler = $view->new_display('calendar_period', 'Day view', 'calendar_period_3');
  $handler->override_option('style_plugin', 'calendar_style');
  $handler->override_option('style_options', array(
    'name_size' => '99',
    'with_weekno' => 0,
    'max_items' => 0,
    'max_items_behavior' => 'more',
    'groupby_times' => 'hour',
    'groupby_times_custom' => '',
    'groupby_field' => '',
  ));
  $handler->override_option('attachment_position', 'after');
  $handler->override_option('inherit_arguments', TRUE);
  $handler->override_option('inherit_exposed_filters', TRUE);
  $handler->override_option('displays', array(
    'calendar_1' => 'calendar_1',
    'default' => 0,
    'calendar_block_1' => 0,
  ));
  $handler->override_option('calendar_type', 'day');
  $handler = $view->new_display('calendar_period', 'Week view', 'calendar_period_4');
  $handler->override_option('style_plugin', 'calendar_style');
  $handler->override_option('style_options', array(
    'name_size' => '99',
    'with_weekno' => 0,
    'max_items' => 0,
    'max_items_behavior' => 'more',
    'groupby_times' => 'hour',
    'groupby_times_custom' => '',
    'groupby_field' => '',
  ));
  $handler->override_option('attachment_position', 'after');
  $handler->override_option('inherit_arguments', TRUE);
  $handler->override_option('inherit_exposed_filters', TRUE);
  $handler->override_option('displays', array(
    'calendar_1' => 'calendar_1',
    'default' => 0,
    'calendar_block_1' => 0,
  ));
  $handler->override_option('calendar_type', 'week');
  $handler = $view->new_display('calendar_period', 'Block view', 'calendar_period_5');
  $handler->override_option('style_plugin', 'calendar_style');
  $handler->override_option('style_options', array(
    'display_type' => 'month',
    'name_size' => '1',
  ));
  $handler->override_option('attachment_position', 'after');
  $handler->override_option('inherit_arguments', TRUE);
  $handler->override_option('inherit_exposed_filters', TRUE);
  $handler->override_option('displays', array(
    'calendar_1' => 0,
    'default' => 0,
    'calendar_block_1' => 'calendar_block_1',
  ));
  $handler->override_option('calendar_type', 'month');
  $handler = $view->new_display('calendar_ical', 'iCal feed', 'calendar_ical_1');
  $handler->override_option('arguments', array());
  $handler->override_option('filters', array(
    'status' => array(
      'operator' => '=',
      'value' => 1,
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'status',
      'table' => 'node',
      'field' => 'status',
      'relationship' => 'none',
    ),
    'date_filter' => array(
      'operator' => '>=',
      'value' => array(
        'value' => NULL,
        'min' => NULL,
        'max' => NULL,
        'default_date' => 'now',
        'default_to_date' => '',
      ),
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'date_fields' => array(
        'node.changed' => 'node.changed',
      ),
      'granularity' => 'day',
      'form_type' => 'date_select',
      'default_date' => 'now',
      'default_to_date' => '',
      'id' => 'date_filter',
      'table' => 'node',
      'field' => 'date_filter',
      'override' => array(
        'button' => 'Use default',
      ),
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('style_plugin', 'ical');
  $handler->override_option('style_options', array(
    'mission_description' => FALSE,
    'description' => '',
    'summary_field' => 'node_title',
    'description_field' => '',
    'location_field' => '',
  ));
  $handler->override_option('row_plugin', '');
  $handler->override_option('path', 'calendar/ical');
  $handler->override_option('menu', array(
    'type' => 'none',
    'title' => '',
    'description' => '',
    'weight' => 0,
    'name' => 'navigation',
  ));
  $handler->override_option('tab_options', array(
    'type' => 'none',
    'title' => '',
    'description' => '',
    'weight' => 0,
  ));
  $handler->override_option('displays', array(
    'calendar_1' => 'calendar_1',
    'default' => 0,
    'calendar_block_1' => 'calendar_block_1',
  ));
  $handler->override_option('sitename_title', FALSE);
  $handler = $view->new_display('block', 'Upcoming', 'block_1');
  $handler->override_option('fields', array(
    'title' => array(
      'label' => '',
      'link_to_node' => 1,
      'exclude' => 0,
      'id' => 'title',
      'field' => 'title',
      'table' => 'node',
      'relationship' => 'none',
      'format' => 'default',
    ),
    'field_location_value' => array(
      'id' => 'field_location_value',
      'table' => 'node_data_field_location',
      'field' => 'field_location_value',
    ),
    'field_datetime_value' => array(
      'id' => 'field_datetime_value',
      'table' => 'node_data_field_datetime',
      'field' => 'field_datetime_value',
    ),
    'field_datetime_value2' => array(
      'id' => 'field_datetime_value2',
      'table' => 'node_data_field_datetime',
      'field' => 'field_datetime_value2',
    ),
    'changed' => array(
      'label' => '',
      'link_to_node' => 0,
      'exclude' => 0,
      'id' => 'changed',
      'field' => 'changed',
      'table' => 'node',
      'relationship' => 'none',
      'date_format' => 'small',
      'format' => 'default',
    ),
  ));
  $handler->override_option('arguments', array());
  $handler->override_option('filters', array(
    'status' => array(
      'operator' => '=',
      'value' => 1,
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'status',
      'table' => 'node',
      'field' => 'status',
      'relationship' => 'none',
    ),
    'date_filter' => array(
      'operator' => '>=',
      'value' => array(
        'value' => NULL,
        'min' => NULL,
        'max' => NULL,
        'default_date' => 'now',
        'default_to_date' => '',
      ),
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'date_fields' => array(
        'node.changed' => 'node.changed',
      ),
      'granularity' => 'day',
      'form_type' => 'date_select',
      'default_date' => 'now',
      'default_to_date' => '',
      'id' => 'date_filter',
      'table' => 'node',
      'field' => 'date_filter',
      'override' => array(
        'button' => 'Use default',
      ),
      'relationship' => 'none',
    ),
    'type' => array(
      'operator' => 'in',
      'value' => array(
        'calendarevent' => 'calendarevent',
      ),
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'type',
      'table' => 'node',
      'field' => 'type',
      'override' => array(
        'button' => 'Use default',
      ),
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('title', 'Upcoming');
  $handler->override_option('items_per_page', 5);
  $handler->override_option('use_more', 1);
  $handler->override_option('style_plugin', 'list');
  $handler->override_option('style_options', array(
    'grouping' => '',
    'type' => 'ul',
  ));
  $handler->override_option('block_description', 'Upcoming');
  $handler->override_option('block_caching', -1);

  $view->save();
}

/*
 * Include a custom view used by the AnyTM installation profile
 *   Latest News is a block that shows any latest news type
 *   nodes
 */

function anytm_include_latestnews_view() {
  views_include('view');

  $view = new view;
  $view->name = 'latestnews';
  $view->description = 'Latest News';
  $view->tag = '';
  $view->view_php = '';
  $view->base_table = 'node';
  $view->is_cacheable = FALSE;
  $view->api_version = 2;
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->override_option('fields', array(
    'title' => array(
      'label' => '',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'link_to_node' => 1,
      'exclude' => 0,
      'id' => 'title',
      'table' => 'node',
      'field' => 'title',
      'relationship' => 'none',
    ),
    'created_1' => array(
      'label' => 'Posted',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'date_format' => 'time ago',
      'custom_date_format' => '',
      'exclude' => 0,
      'id' => 'created_1',
      'table' => 'node',
      'field' => 'created',
      'override' => array(
        'button' => 'Override',
      ),
      'relationship' => 'none',
    ),
    'body' => array(
      'label' => '',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'exclude' => 0,
      'id' => 'body',
      'table' => 'node_revisions',
      'field' => 'body',
      'override' => array(
        'button' => 'Override',
      ),
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('filters', array(
    'type' => array(
      'operator' => 'in',
      'value' => array(
        'newsitem' => 'newsitem',
      ),
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'type',
      'table' => 'node',
      'field' => 'type',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'none',
  ));
  $handler->override_option('cache', array(
    'type' => 'none',
  ));
  $handler->override_option('title', 'Latest News');
  $handler->override_option('items_per_page', 5);
  $handler = $view->new_display('block', 'Block', 'block_1');
  $handler->override_option('fields', array(
    'title' => array(
      'label' => '',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'link_to_node' => 1,
      'exclude' => 0,
      'id' => 'title',
      'table' => 'node',
      'field' => 'title',
      'relationship' => 'none',
    ),
    'created_1' => array(
      'label' => 'Posted',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'date_format' => 'time ago',
      'custom_date_format' => '',
      'exclude' => 0,
      'id' => 'created_1',
      'table' => 'node',
      'field' => 'created',
      'override' => array(
        'button' => 'Override',
      ),
      'relationship' => 'none',
    ),
    'teaser' => array(
      'label' => 'Teaser',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 1,
        'max_length' => '40',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 1,
        'html' => 0,
      ),
      'exclude' => 0,
      'id' => 'teaser',
      'table' => 'node_revisions',
      'field' => 'teaser',
      'override' => array(
        'button' => 'Use default',
      ),
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('block_description', '');
  $handler->override_option('block_caching', -1);
  $handler = $view->new_display('page', 'Page', 'page_2');
  $handler->override_option('items_per_page', 0);
  $handler->override_option('path', 'news');
  $handler->override_option('menu', array(
    'type' => 'none',
    'title' => '',
    'description' => '',
    'weight' => 0,
    'name' => 'navigation',
  ));
  $handler->override_option('tab_options', array(
    'type' => 'none',
    'title' => '',
    'description' => '',
    'weight' => 0,
  ));

  $view->save();
}

/*
 * Remove local variables used by the AnyTM installation profile
 */

function anytm_cleanup() {
  variable_del('anytm_block_calendar_placement');
  variable_del('anytm_block_calendar_enabled');
  variable_del('anytm_block_configuration_finished');
  variable_del('anytm_block_drupal_footer_enabled');
  variable_del('anytm_block_drupal_footer_placement');
  variable_del('anytm_block_forum_active_enabled');
  variable_del('anytm_block_forum_active_placement');
  variable_del('anytm_block_forum_enabled');
  variable_del('anytm_block_forum_new_enabled');
  variable_del('anytm_block_forum_new_placement');
  variable_del('anytm_block_latestnews_enabled');
  variable_del('anytm_block_latestnews_placement');
  variable_del('anytm_block_logon_enabled');
  variable_del('anytm_block_logon_placement');
  variable_del('anytm_block_menu_primary_enabled');
  variable_del('anytm_block_menu_primary_placement');
  variable_del('anytm_block_menu_secondary_enabled');
  variable_del('anytm_block_menu_secondary_placement');
  variable_del('anytm_block_navigation_enabled');
  variable_del('anytm_block_navigation_placement');
  variable_del('anytm_block_poll_enabled');
  variable_del('anytm_block_poll_placement');
  variable_del('anytm_block_shoutbox_enabled');
  variable_del('anytm_block_shoutbox_placement');
  variable_del('anytm_block_upcoming_enabled');
  variable_del('anytm_block_upcoming_placement');
  variable_del('anytm_block_whosnew_enabled');
  variable_del('anytm_block_whosnew_placement');
  variable_del('anytm_block_whosonline_enabled');
  variable_del('anytm_block_whosonline_placement');
  variable_del('anytm_content_setup_finished');
  variable_del('anytm_date_configuration_finished');
  variable_del('anytm_final_finished');
  variable_del('anytm_forum_finished');
  variable_del('anytm_frontpage_finished');
  variable_del('anytm_intro_finished');
  variable_del('anytm_placement_array');
  variable_del('anytm_poll_finished');
  variable_del('anytm_setup_users');
  variable_del('anytm_shoutbox_finished');
  variable_del('anytm_themes');
  variable_del('anytm_theme_array');
  variable_del('anytm_theme_options_array');
  variable_del('anytm_theme_selected');
  variable_del('anytm_theme_selection_finished');
  variable_del('anytm_user_array_exists');
  variable_del('anytm_user_setup1_finished');
  variable_del('anytm_user_setup2_finished');
  variable_del('anytm_user_setup_array');
}